//
//  Utilities.h
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/17/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _Utilities_H_
#define _Utilities_H_
#include <string>
#include <sstream>
namespace tin{
    class Utilities {
        public:
        static std::string StringByReplacing(std::string string, char input, std::string replace){
            size_t pos = std::string::npos;
            do {
                pos = string.find(input);
                if (pos != std::string::npos){
                    string.replace(pos, 1, replace);
                }
            } while (pos != std::string::npos);
            return string;
        }
        
            template <typename T>
            static std::string NumberToString (T number){
                std::ostringstream ss;
                ss << number;
                return ss.str();
            }
        private:
    };
} // tin
#endif
