//
//  Post.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 08/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "Post.h"

namespace tin {
    void Post::guid(std::string guid){
        guid_ = guid;
    }
    
    bool Post::operator==(const Post &rhs) const{
        if (rhs.guid_ == this->guid_){
            return true;
        }
        return false;
    }
    
    bool Post::operator!=(const Post &rhs) const{
        return !this->operator==(rhs);
    }
    
    void Post::ClearAttachments(){
        attachment_ids_.clear();
    }
    
    void Post::AddAttachment(std::string content_id){
        attachment_ids_.insert(content_id);
    }
    
    std::vector<std::string> Post::attachments(){
        return std::vector<std::string>(attachment_ids_.begin(), attachment_ids_.end());
    }
    
    Json::Value Post::RegionsToJson(){
        Json::Value root;
        auto parser = Json::Reader();
        parser.parse(regions(), root);
        return root;
    }
    
    Json::Value Post::ExtensionsToJson(){
        Json::Value root;
        auto parser = Json::Reader();
        parser.parse(extensions(), root);
        return root;
    }
}