//
//  Page.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef PiranhaCMS_Page_h
#define PiranhaCMS_Page_h
#include <string>
#include <set>
#include <vector>
#include <json/json.h>
namespace tin {
    class Page{
    public:
        Page() : title_(""), guid_(""), parent_guid_(""), order_id_(0), sync_(false), is_hidden_(false) {};
        
        /**
         Gets & sets the page id
         */
        std::string guid() { return guid_; };
        void        guid(std::string id);
        
        /**
         Getter & Setters for the parent id from the CMS
         */
        std::string parent_id() { return parent_guid_; };
        void        parent_id(std::string parent_id) { parent_guid_ = parent_id; };
        
        /**
         Getter & Setters for the title
         */
        std::string title() { return title_; };
        void        title(std::string title) { title_ = title; };
        
        std::string template_name() { return template_name_; };
        void        template_name(std::string template_name) { template_name_ = template_name; };
        
        /**
         Clears all the attachment ids
         */
        void        ClearAttachments();
        
        /**
         Adds an attachment with the given content id
         */
        void        AddAttachment(std::string content_id);
        
        /**
         Returns a vector with the content ids of the page
         */
        std::vector<std::string> attachments();
        
        /**
         The regions of the page as a serialized json object
         */
        std::string regions() { return regions_; };
        void        regions(std::string regions) { regions_ = regions; };
        
        std::string extensions() {return extensions_; };
        void        extensions(std::string extensions) { extensions_ = extensions; }
        
        Json::Value RegionsToJson();
        Json::Value ExtensionsToJson();
        
        /**
         Sync is used to determine if attachments are downloaded & updated
         */
        bool        sync() { return sync_; };
        void        sync(bool sync) { sync_ = sync; };
        
        bool        is_hidden() { return is_hidden_; };
        void        is_hidden(bool is_hidden) { is_hidden_ = is_hidden; };
        
        /**
         Operator checks if page id's are equal
         */
        bool        operator==(const Page &rhs) const;
        
        /**
         Operator checks if page id's are not equal
         */
        bool        operator!=(const Page &rhs) const;
        
        int         order_id(){ return order_id_; };
        void        order_id(int order_id){ order_id_ = order_id; };
        
    private:
        std::string             title_;
        std::string             guid_;
        std::string             parent_guid_;
        std::set<std::string>   attachment_ids_;
        std::string             template_name_;
        std::string             regions_;
        std::string             extensions_;
        bool                    is_hidden_;
        bool                    sync_;
        int                     order_id_;        
    };
}

#endif
