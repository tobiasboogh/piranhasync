//
//  Settings.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _Settings_H_
#define _Settings_H_
#include <string>

namespace tin{
    class Settings{
        public:
            Settings() : latest_update_timestamp_("1970-01-01/01-01-01"), language_("default_site"){ };
            Settings(std::string latest_update_timestamp, std::string language) { latest_update_timestamp_ = latest_update_timestamp; language_ = language; };
            std::string latest_update_timestamp() { return latest_update_timestamp_; };
            void        latest_update_timestamp(std::string latest_update_timestamp) { latest_update_timestamp_ = latest_update_timestamp; };
            
            std::string language() { return language_; };
            void        language(std::string language) { language_ = language; };
        private:
            std::string latest_update_timestamp_;
            std::string language_;
            std::string root_page_;
    };
} // tin
#endif
