//
//  Content.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _Content_H_
#define _Content_H_
#include <string>

namespace tin{
    typedef enum ContentType{
        CONTENT_TYPE_UNKNOWN    = 0,
        CONTENT_TYPE_PDF        = 1,
        CONTENT_TYPE_VIDEO      = 2,
        CONTENT_TYPE_IMAGE      = 3,
        CONTENT_TYPE_AR         = 4
    } ContentType;
    
    typedef enum {
        NOT_DOWNLOADED = 0,
        NEEDS_DOWNLOAD,
        DOWNLOADED
    } DownloadStatus;
    
    class Content{
    public:
        std::string     guid() { return guid_; };
        void            guid(std::string guid) {guid_ = guid; };
        
        bool            is_image() { return is_image_; };
        void            is_image(bool is_image) { is_image_ = is_image; };
        
        std::string     mime_type() { return mime_type_; };
        void            mime_type(std::string mime_type) { mime_type_ = mime_type; };
        
        std::string     file_name() { return file_name_; };
        std::string     file_name() const { return file_name_; };
        void            file_name(std::string file_name) { file_name_ = file_name; };
        
        std::string     content_url() { return content_url_; };
        void            content_url(std::string content_url) { content_url_ = content_url; };
        
        std::string     thumbnail_url() { return thumbnail_url_; };
        void            thumbnail_url(std::string thumbnail_url) { thumbnail_url_ = thumbnail_url; };
        
        std::string     display_name() { return display_name_; };
        void            display_name(std::string display_name) { display_name_ = display_name; };
        
        std::string     description() { return description_; };
        void            description(std::string description) { description_ = description; };
        
        ContentType     type() { return type_; };
        void            type(ContentType type) { type_ = type; };
        
        int             download_status() { return download_status_; };
        void            download_status(int download_status) { download_status_ = download_status; };
        
        bool            ignore() { return ignore_; }
        void            ignore(bool ignore) { ignore_ = ignore; };
        
        void            updated(std::string updated){ updated_ = updated; };
        std::string     updated(){ return updated_; };
        
        bool            flagged() { return flagged_; };
        void            flagged(bool flagged) { flagged_ = flagged; };
    private:
        std::string     guid_;
        bool            is_image_;
        std::string     mime_type_;
        std::string     file_name_;
        std::string     content_url_;
        std::string     thumbnail_url_;
        std::string     display_name_;
        std::string     description_;
        std::string     updated_;
        int             download_status_;
        bool            ignore_;
        bool            flagged_;
        ContentType     type_;
    };
} // tin
#endif
