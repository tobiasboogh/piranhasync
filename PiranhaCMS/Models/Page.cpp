//
//  PiranhaClasses.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "Page.h"
namespace tin{
    void Page::guid(std::string guid){
        guid_ = guid;
    }

    bool Page::operator==(const Page &rhs) const{
        if (rhs.guid_ == this->guid_){
            return true;
        }
        return false;
    }

    bool Page::operator!=(const Page &rhs) const{
        return !this->operator==(rhs);
    }

    void Page::ClearAttachments(){
        attachment_ids_.clear();
    }

    void Page::AddAttachment(std::string content_id){
        attachment_ids_.insert(content_id);
    }

    std::vector<std::string> Page::attachments(){
        return std::vector<std::string>(attachment_ids_.begin(), attachment_ids_.end());
    }
    
    Json::Value Page::RegionsToJson(){
        Json::Value root;
        auto parser = Json::Reader();
        parser.parse(regions(), root);
        return root;
    }
    
    Json::Value Page::ExtensionsToJson(){
        Json::Value root;
        auto parser = Json::Reader();
        parser.parse(extensions(), root);
        return root;
    }
}