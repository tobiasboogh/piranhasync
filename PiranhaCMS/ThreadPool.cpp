//
//  ThreadPool.cpp
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "ThreadPool.h"
#ifdef ANROID
#include <android/log.h>
#endif
namespace tin {
    void Worker::operator()(){
        std::function<void()> task;
        while (true){
            {
                std::unique_lock<std::mutex> lock(pool_.queue_mutex_);
                while (!pool_.stop_ && pool_.tasks_.empty()) {
                    pool_.condition_variable_.wait(lock);
                }
                
                if (pool_.stop_){
                    return;
                }
                
                task = pool_.tasks_.front();
                pool_.tasks_.pop_front();
            }
            task();
            pool_.condition_variable_.notify_all();
        }
        
    }
    
    void ThreadPool::WaitUntilEmpty(){
        std::unique_lock<std::mutex> lock(queue_mutex_);
        while (!stop_ && !tasks_.empty()){
            condition_variable_.wait(lock);
        }
    }
    
    ThreadPool::ThreadPool(size_t num_threads) : stop_(false){
        for (size_t i =0; i < num_threads; i++){
            workers_.push_back(std::thread(Worker(*this)));
        }
    }
    
    ThreadPool::~ThreadPool(){
        stop_ = true;
#ifdef ANDROID
        __android_log_print(ANDROID_LOG_INFO, "tin::Info", "Stop");
#endif
        condition_variable_.notify_all();
#ifdef ANDROID
        __android_log_print(ANDROID_LOG_INFO, "tin::Info", "Notify All");
#endif
        for (size_t i=0; i < workers_.size(); i++){
#ifdef ANDROID
            __android_log_print(ANDROID_LOG_INFO, "tin::Info", "Join %d", i);
#endif
            workers_[i].join();

        }
    }
}