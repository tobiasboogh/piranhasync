//
//  Delegate.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 29/10/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef PiranhaCMS_Delegate_h
#define PiranhaCMS_Delegate_h

#include "Models/Page.h"
#include "Models/Post.h"
namespace tin{
    class PiranhaCMS;
    class Delegate{
    public:
        ~Delegate() { };
        virtual void OnPageProcessed(PiranhaCMS *cms, Page page) { };
        virtual void OnPostProcessed(PiranhaCMS *cms, Post post) { };
        virtual void OnSitemapProcessed(PiranhaCMS *cms) { };
        virtual void OnChangesProcessed(PiranhaCMS *cms) { };
        
        virtual bool OnPageProcessedNotification() { return false; };
        virtual bool OnPostProcessedNotification() { return false; };
        
        virtual void DidToggleSyncForPage(Page page) {};
    };
}

#endif
