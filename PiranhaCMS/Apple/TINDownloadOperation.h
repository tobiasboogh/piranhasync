//
//  TINDownloadOperation.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 21/05/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <PiranhaCMS.h>
@interface TINDownloadOperation : NSOperation
-(id)initWithUrl:(NSURL *)url returnResult:(BOOL)returnResult fileName:(NSString *)fileName callback:(tin::DownloadQueueCompletionCallback)callback guid:(NSString *)guid;
@end
