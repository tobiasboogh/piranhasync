//
//  TINDownloadOperation.m
//  PiranhaCMS
//
//  Created by Tobias Boogh on 21/05/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import "TINDownloadOperation.h"
#import <Categories/NSString+STLString.h>
#import "TINPiranahaCMSUtils.h"

@interface TINDownloadOperation()
@property (nonatomic, strong) NSURL *url;
@property (nonatomic) BOOL returnResult;
@property (nonatomic, strong) NSString *filename;
@property (nonatomic) tin::DownloadQueueCompletionCallback callback;
@property (nonatomic, strong) NSString *guid;
@property (nonatomic) BOOL running;
@property (nonatomic) BOOL done;
@end

@implementation TINDownloadOperation
-(id)initWithUrl:(NSURL *)url returnResult:(BOOL)returnResult fileName:(NSString *)fileName callback:(tin::DownloadQueueCompletionCallback)callback guid:(NSString *)guid{
    self = [super init];
    if (self){
        self.url = url;
        self.returnResult = returnResult;
        self.filename = fileName;
        self.callback = callback;
        self.guid = guid;
        self.done = NO;
        self.running = NO;
    }
    return self;
}

-(void)main{
    self.running = YES;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:self.url];
    [request setValue:[NSString stringWithSTLString:tin::PiranhaCMS::SharedInstance()->GetVersion()] forHTTPHeaderField:@"SyncEngine"];
    
    if (self.returnResult){
        NSURLSessionDataTask *dataTask = [[[TINPiranahaCMSUtils getInstance] urlSessionManager] dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
            if (error){
                NSLog(@"%@", error);
                self.callback(false, [[self.url path] stlString], "", "", "");
            }
            if ([responseObject isKindOfClass:[NSDictionary class]]){
                NSData *data = [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:nil];
                NSString *stringData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                self.callback(true, [[self.url path] stlString], "", "", [stringData cStringUsingEncoding:NSUTF8StringEncoding]);
            } else {
                self.callback(false, [[self.url path] stlString], "", "", "");
            }
            [self completeOperation];
        }];
        
        [dataTask resume];
    } else {
        NSProgress *progress = nil;
        
        NSURLSessionDownloadTask *downloadTask = [[[TINPiranahaCMSUtils getInstance] urlSessionManager] downloadTaskWithRequest:request progress:&progress destination:^NSURL *(NSURL *targetPath, NSURLResponse *response) {
            NSURL *fileURL = [NSURL fileURLWithPath:self.filename];
            return fileURL;
        } completionHandler:^(NSURLResponse *response, NSURL *filePath, NSError *error) {
            if (error){
                NSLog(@"%@", error);
                self.callback(false, [[self.url path] stlString], "", [self.guid stlString],"");
            } else {
                self.callback(true, [[self.url path] stlString], "", [self.guid stlString],"");
            }
            [self completeOperation];
            
        }];
        [progress setUserInfoObject:[self.filename lastPathComponent] forKey:@"filename"];
        [progress setUserInfoObject:self.guid forKey:@"guid"];
        [progress setUserInfoObject:[NSNumber numberWithInt:downloadTask.taskIdentifier] forKey:@"taskIdentifier"];
        [progress addObserver:[TINPiranahaCMSUtils getInstance] forKeyPath:@"fractionCompleted" options:0 context:nil];
        
        [[[TINPiranahaCMSUtils getInstance] progressArray] addObject:progress];
        
        TINPiranahaCMSUtils *utils = [TINPiranahaCMSUtils getInstance];
        if (utils.progressChangedBlock != nil){
            utils.progressChangedBlock(progress, kDownloadProgressUpdateAdded);
        }
        
        [downloadTask resume];
    }
}

-(void)start{
    if (![self isCancelled]) {
        [self main];
    }
}

-(BOOL)isConcurrent{
    return YES;
}

-(BOOL)isExecuting{
    return self.running;
}

-(BOOL)isFinished{
    return self.done;
}

-(void)setDone:(BOOL)done{
    [self willChangeValueForKey:@"isFinished"];
    _done = done;
    [self didChangeValueForKey:@"isFinished"];
}

-(void)setRunning:(BOOL)running{
    [self willChangeValueForKey:@"isExecuting"];
    _running = running;
    [self didChangeValueForKey:@"isExecuting"];
}

- (void)completeOperation {
    self.running = NO;
    self.done = YES;
}
@end
