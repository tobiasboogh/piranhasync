//
//  FDCFileResolver.h
//  Catalog
//
//  Created by Tobias Boogh on 30/10/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vendor/AFNetworking/AFNetworking/AFURLSessionManager.h"
#include "PiranhaCMS.h"
#import "Categories/NSString+STLString.h"
#include "PiranhaCMS.h"
#include "PiranhaClasses.h"

typedef NS_ENUM(NSUInteger, DownloadProgressChangedEnum) {
    kDownloadProgressUpdateAdded,
    kDownloadProgressUpdateRemoved
};

typedef void(^DownloadProgressBlock)(float value);
typedef void(^DownloadProgressChanged)(NSProgress *progress, DownloadProgressChangedEnum state);
typedef void(^CompletionBlock)(bool);


@interface TINPiranahaCMSUtils : NSObject
@property (nonatomic, strong) AFURLSessionManager *urlSessionManager;
@property (nonatomic, strong) NSMutableArray *progressArray;
@property (nonatomic, strong) NSMutableDictionary *progressCompletionDict;
@property (readwrite, copy) DownloadProgressChanged progressChangedBlock;
@property (nonatomic, readonly) NSOperationQueue *downloadQueue;

+(NSString *)documentFilePath:(NSString *)fileName;
+(NSString *)documentsPath;

+(NSString *)resolveURLForFileName:(NSString *)fileName;

+(NSDictionary *)dictionaryForJSONString:(std::string)data_string;
+(NSString *)documentFilePathSTL:(std::string)fileName;

+(TINPiranahaCMSUtils *)getInstance;
-(void)setSite:(NSString *)site andDelegate:(tin::Delegate *)delegate;

-(void)getChangesWithCompletionBlock:(CompletionBlock)completion;
@end