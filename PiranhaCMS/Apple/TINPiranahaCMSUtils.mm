//
//  FDCFileResolver.m
//  Catalog
//
//  Created by Tobias Boogh on 30/10/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#import "TINPiranahaCMSUtils.h"
#import "TINDownloadOperation.h"

@interface TINPiranahaCMSUtils(){
    
}
@property (nonatomic, strong) NSOperationQueue *downloadQueue;
@end

@implementation TINPiranahaCMSUtils

+(TINPiranahaCMSUtils *)getInstance{
    static dispatch_once_t onceToken;
    static TINPiranahaCMSUtils* sharedInstance = nil;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[TINPiranahaCMSUtils alloc] init];
    });
    return sharedInstance;
}

-(id)init{
    self = [super init];
    if (self){
        self.downloadQueue = [[NSOperationQueue alloc] init];
        self.downloadQueue.maxConcurrentOperationCount = 4;
        
        NSLog(@"%@", [@"~/Documents" stringByExpandingTildeInPath]);
        
    }
    return self;
}

-(void)getChangesWithCompletionBlock:(CompletionBlock)completion{
    tin::PiranhaCMS::SharedInstance()->GetLatestChangesAsync(^(bool success, std::string url, std::string filename, std::string guid, std::string string_data){
        dispatch_async(dispatch_get_main_queue(), ^{
            completion(success); 
        });
    });
}

-(void)setSite:(NSString *)site andDelegate:(tin::Delegate *)delegate{
    static NSString *databaseFileName = @"piranhacms.sqlite";
    
    NSString *databaseDocumentsPath = [TINPiranahaCMSUtils documentFilePath:databaseFileName];
    NSFileManager *manager = [[NSFileManager alloc] init];
    if (![manager fileExistsAtPath:databaseDocumentsPath]){
        NSString *pathToResource = [[NSBundle mainBundle] pathForResource:databaseFileName ofType:nil];
        NSError *copyError = nil;
        if (pathToResource != nil){
            [manager copyItemAtPath:pathToResource toPath:databaseDocumentsPath error:&copyError];
            if (copyError != nil){
                NSLog(@"%@", copyError);
                abort();
            }
        }
    }
    
    tin::PiranhaCMS::Create();
    
    NSString *appPath = [@"~/Documents" stringByExpandingTildeInPath];
    std::string base_path = [appPath cStringUsingEncoding:NSUTF8StringEncoding];
    
    tin::PiranhaCMSOptions options;
    options.max_connections = 2;
    options.base_path = base_path;
    options.database_path = base_path + "/piranhacms.sqlite";
    options.site_url = [site UTF8String];
    options.delegate = delegate;
    tin::PiranhaCMS::SharedInstance()->SetOptions(options);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    self.urlSessionManager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    [self.urlSessionManager setResponseSerializer:[[AFJSONResponseSerializer alloc] init]];
    
    self.progressArray = [[NSMutableArray alloc] init];
    self.progressCompletionDict = [[NSMutableDictionary alloc] init];
}

-(void)dealloc{
    tin::PiranhaCMS::Destroy();
}

+(NSString *)documentFilePath:(NSString *)fileName{
    return [[TINPiranahaCMSUtils documentsPath] stringByAppendingPathComponent:fileName];
}

+(NSString *)documentFilePathSTL:(std::string)fileName{
    return [[TINPiranahaCMSUtils documentsPath] stringByAppendingPathComponent:[NSString stringWithCString:fileName.c_str() encoding:NSUTF8StringEncoding]];
}

+(NSString *)documentsPath{
    return [@"~/Documents" stringByExpandingTildeInPath];
}

+(NSString *)resolveURLForFileName:(NSString *)fileName{
    if ([[NSFileManager defaultManager] fileExistsAtPath:[TINPiranahaCMSUtils documentFilePath:fileName]]){
        return [TINPiranahaCMSUtils documentFilePath:fileName];
    }
    NSString *bundlePath = [[NSBundle mainBundle] pathForResource:fileName ofType:nil];
    if (bundlePath == nil){
        NSLog(@"Missing file %@", fileName);
    }
    return bundlePath;
}

+(NSDictionary *)dictionaryForJSONString:(std::string)data_string{
    NSArray *regionsArray = [NSJSONSerialization JSONObjectWithData:[NSData dataWithBytes:data_string.c_str() length:data_string.length()] options:0 error:nil];
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    for (NSDictionary *regionDict in regionsArray){
        id name = regionDict[@"Name"];
        id body = regionDict[@"Body"];
        [dict setObject:body forKey:name];
    }
    return dict;
}

void tin::DownloadFileFromURL(std::string url, bool return_result, std::string filename, std::string guid, DownloadQueueCompletionCallback callback){
    NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithSTLString:url]];
    TINDownloadOperation *downloadOperation = [[TINDownloadOperation alloc] initWithUrl:nsUrl returnResult:return_result fileName:[NSString stringWithSTLString:filename] callback:callback guid:[NSString stringWithSTLString:guid]];

    [[[TINPiranahaCMSUtils getInstance] downloadQueue] addOperation:downloadOperation];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context{
    if ([keyPath isEqualToString:@"fractionCompleted"]){
        NSProgress *progress = (NSProgress *)object;
        DownloadProgressBlock progressBlock = [progress.userInfo valueForKey:@"progressBlock"];
        if (progressBlock != nil) {
            progressBlock(progress.fractionCompleted);
        }
        if (progress.fractionCompleted == 1.0f){
            [[self progressArray] removeObject:progress];
            if (self.progressChangedBlock != nil){
                self.progressChangedBlock(progress, kDownloadProgressUpdateRemoved);
            }
        }
    } else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

+(void)removeFileAtPath:(NSString *)filePath{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL isDirectory = NO;
    if ([fileManager fileExistsAtPath:filePath isDirectory:&isDirectory]){
        if (isDirectory == NO){
            NSError *error = nil;
            [fileManager removeItemAtPath:filePath error:&error];
            NSLog(@"Removing: %@", filePath);
            if (error != nil){
                NSLog(@"%@", error);
            }
        }
    }
}

void tin::DeleteFile(std::string file_path){
    [TINPiranahaCMSUtils removeFileAtPath:[NSString stringWithCString:file_path.c_str() encoding:NSUTF8StringEncoding]];
}
@end
