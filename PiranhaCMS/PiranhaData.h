//
//  PiranhaData.h
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _PiranhaData_H_
#define _PiranhaData_H_

#include <map>
#include <list>
#include <string>
#include "Singleton.h"

#include "PiranhaClasses.h"
#include <sqlite3.h>

namespace tin{
    typedef enum ContentTypeBits{
        CONTENT_TYPE_BIT_UNKNOWN    = (1u << 0),
        CONTENT_TYPE_BIT_PDF        = (1u << 1),
        CONTENT_TYPE_BIT_VIDEO      = (1u << 2),
        CONTENT_TYPE_BIT_IMAGE      = (1u << 3),
        CONTENT_TYPE_BIT_AR         = (1u << 4)
    } ContentTypeBits;
    
    class PiranhaData : public Singleton<PiranhaData>{
        public:
            /**
             Opens or creates a database at the path and creates the necessary tables
             @param full_path The path to database file
             */
            void                        Setup(std::string full_path);
        
            /**
             Adds or updates the given page
             @param page The page to add or update
             */
            void                        AddOrUpdatePage(Page page);
        
            /**
             Removes the page with given id
             @param page_id The id of the page to remove
             */
            void                        RemovePage(std::string page_id);
        
            /**
             Get the page from database with the given id
             @param page_id The id of the page
             @return Page object for the requested page
             */
            Page                        GetPageWithId(std::string page_id);

        
            /**
             Gets all child pages for the page with given id
             @param page_id The parent page id
             @return A vector of Page objects that are the children of the parent page with the given page id
             */
            std::vector<Page>           GetChildPagesForPageWithId(std::string page_id);
        
        
            std::vector<Page>           GetPagesWithTemplate(std::string template_name);
            /**
             Adds the content to the database
             @param content The content object to be added to the database
             */
            void                        AddContent(Content content);
        
            /**
             Removes the content with the given id from the database
             */
            void                        RemoveContent(std::string content_id);
        
            /**
             Gets a content object for the given content id
             @return A content object for the given id
             */
            Content                     GetContentWithId(std::string content_id);
        
            /**
             
             */
            std::vector<Content>        GetContentsForType(ContentType content_type);
            std::vector<Content>        GetContentForPageWithId(std::string page_id, ContentType type);
        
        
            /**
             Adds a relation between the given page id and the content id.
             @param page_id The page id to associate
             @param content_id The content id to associate
             */
            void                        AddPageContent(std::string page_id, std::string content_id);
        
            /**
             Removes all content relations for the give page id
             @param page_id The page id to clear
             */
            void                        RemovePageContentForPage(std::string page_id);
        
            /**
             Remove all relations from all pages to given content id
             @param content_id The content id to remove relations
             */
            void                        RemovePageContentForContent(std::string content_id);
        
            /**
             Gets all content ids associated to a page
             @param page_id The page id to get content ids for
             @return A vector with string content ids for the page with the given id
             */
            std::vector<std::string>    ContentIdsForPage(std::string page_id);
        
            /**
             Gets all content ids in the database
             @return A vector with string content ids for the page with the given id
             */
            std::list<std::string>      AllContentIds();
        
            /**
             Updates the timestamp in the database
             @param timestamp The string timestamp to update
             */
//            void                        UpdateTimestamp(std::string timestamp);
        
            /**
             Gets the timestamp stored in the database
             @return The string timestamp
             */
//            std::string                 GetTimestamp();
        
            /**
             Gets the current language
             @return String with the language identifier
             */
            std::string                 GetCurrentLanguage();
        
            /**
             Sets the current language
             @param language The language string
             */
            void                        SetCurrentLanguage(std::string language);
        
        
            std::vector<Content>        UnfinishedContent();
            std::vector<Content>        GetContentForPageWithId(std::string page_id);
        
            Settings                    GetSettings();
            void                        UpdateSettings(Settings settings);
        
        
            void                        ClearPages();
            void                        ClearContentForTypes(int types, std::string base_path);
        
            void                        CreateTables();
            void                        ResetTables();
        
            void                        SetAllContentToIgnore(bool ignore);
        private:
            sqlite3 *database;
        
            void                        Init();
            void                        Shutdown();
        
            void                        ExecuteSqlQuery(std::string sql_string);
        
            int                         GetUserVersion();
            void                        SetUserVersion(int version);
        
            Page                        PageFromSQLString(std::string string);
            std::vector<Page>           PagesFromSQLString(std::string string);
            Content                     ContentFromSQLString(std::string string);
            std::vector<Content>        ContentsFromSQLString(std::string string);
            void                        PageFromStatement(Page &page, sqlite3_stmt *statement);
            friend class Singleton;
    };
} // tin
#endif
