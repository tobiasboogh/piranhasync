//
//  Database.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _Database_H_
#define _Database_H_
#include <string>
#include <iostream>
#include <sstream>
#include <sqlite3.h>

namespace tin{
    class Database {
        public:
            Database(std::string database_path);
            bool    ExecuteSqlQuery(std::string sql_string);
            int     GetUserVersion();
            void    SetUserVersion(int version);
        
            bool    ExecuteEnumerateRows(std::string sql, std::function<void(sqlite3_stmt *)> callback);
        
            static std::string EscapeSQLString(const std::string& input);
        private:
            sqlite3 *database_;
        
            bool CheckSqliteError(int result, sqlite3 *database);
        
    };
} // tin
#endif
