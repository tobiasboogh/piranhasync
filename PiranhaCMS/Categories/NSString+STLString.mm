//
//  NSString+STLString.m
//  PiranhaCMS
//
//  Created by Tobias Boogh on 04/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import "NSString+STLString.h"


@implementation NSString (STLString)
+(NSString *)stringWithSTLString:(std::string)string{
    return [NSString stringWithCString:string.c_str() encoding:NSUTF8StringEncoding];
}

-(std::string)stlString{
    return std::string([self cStringUsingEncoding:NSUTF8StringEncoding]);
}
@end
