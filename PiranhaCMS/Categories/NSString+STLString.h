//
//  NSString+STLString.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 04/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#include <string>
@interface NSString (STLString)
+(NSString *)stringWithSTLString:(std::string)string;
-(std::string)stlString;
@end
