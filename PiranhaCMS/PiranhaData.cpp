//
//  PiranhaData.cpp
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "PiranhaData.h"
#include <iostream>
#include <sstream>
#include <assert.h>
#include "Utilities.h"
namespace tin {
    bool CheckSqliteError(int result, sqlite3 *database){
        if (result != SQLITE_OK){
            std::cout << sqlite3_errmsg(database) << std::endl;
            return false;
        }
        return true;
    }
    
    std::string escapeSQLString(const std::string& input) {
        std::ostringstream ss;
        for (auto iter = input.cbegin(); iter != input.cend(); iter++) {
            switch (*iter) {
                case '\'': ss << "''"; break;
                default: ss << *iter; break;
            }
        }
        return ss.str();
    }
    
    void PiranhaData::Init(){
        
    }
    
    void PiranhaData::Setup(std::string full_path){
        int result = sqlite3_config(SQLITE_CONFIG_SERIALIZED);
        CheckSqliteError(result, database);
        result = sqlite3_initialize();
        if (!CheckSqliteError(result, database)){
            std::cout << "ERROR INITILIAZING SQLITE" << std::endl;
        }
        if (!CheckSqliteError(sqlite3_open(full_path.c_str(), &database), database)){
            std::cout << "ERROR OPENING DATABASE" << std::endl;
        }
        CreateTables();
    }
    
    void PiranhaData::Shutdown(){
        int result = 0;
        result = sqlite3_close(database);
        CheckSqliteError(result, database);
        result = sqlite3_shutdown();
        CheckSqliteError(result, database);
    }
    
    void PiranhaData::CreateTables(){
        std::string create_pages = "CREATE TABLE IF NOT EXISTS Page (Id TEXT NOT NULL PRIMARY KEY, Parentid TEXT, Title TEXT, Regions TEXT, Sync INTEGER, OrderId INTEGER, TemplateName TEXT)";
        std::string create_posts = "CREATE TABLE IF NOT EXISTS Post (Id TEXT NOT NULL PRIMARY KEY, Parentid TEXT, Title TEXT, Regions TEXT, Sync INTEGER, OrderId INTEGER, TemplateName TEXT, Extensions TEXT)";
        
        std::string create_page_content = "CREATE TABLE IF NOT EXISTS PageContent (PageId TEXT NOT NULL, ContentId TEXT NOT NULL)";
        std::string create_content = "CREATE TABLE IF NOT EXISTS Content (Id TEXT NOT NULL PRIMARY KEY, MimeType TEXT, FileName TEXT, ContentURL TEXT, ThumbnailURL TEXT, DisplayName TEXT, Description TEXT, Downloaded INTEGER, Type INTEGER, Updated TEXT, Ignore INTEGER)";
        
        Settings settings;
        std::string create_settings = "CREATE TABLE IF NOT EXISTS Settings (LatestUpdateTimeStamp TEXT NOT NULL default '" + settings.latest_update_timestamp() + "', Language TEXT NOT NULL default '" + settings.language() + "')";
        
        ExecuteSqlQuery(create_settings);
        ExecuteSqlQuery(create_pages);
        ExecuteSqlQuery(create_page_content);
        ExecuteSqlQuery(create_content);
        
        int version = GetUserVersion();
        if (version == 0){
            std::string add_content_ignore = "ALTER TABLE Content ADD COLUMN Ignore INTEGER";
            ExecuteSqlQuery(add_content_ignore);
        }
        if (version == 1){
            std::string add_page_extension = "ALTER TABLE Page ADD COLUMN Extensions TEXT";
            ExecuteSqlQuery(add_page_extension);
        }
        SetUserVersion(2);
    }
    
    void PiranhaData::SetUserVersion(int version){
        char buffer[512];
        sprintf(buffer, "PRAGMA user_version=%d;", version);
        std::string sql(buffer);
        ExecuteSqlQuery(sql);
    }
    
    int PiranhaData::GetUserVersion(){
        int version = 0;
        
        std::string sql = "PRAGMA user_version;";
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare_v2(database, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database)){
            std::cout << "Error with: " << sql << std::endl;
        } else {
            int step = 0;
            while ((step = sqlite3_step(statement)) == SQLITE_ROW){
                version = sqlite3_column_int(statement, 0);
            };
            sqlite3_finalize(statement);
        }
        return version;
    }
    
    void PiranhaData::ExecuteSqlQuery(std::string sql_string){
        char *errmsg = nullptr;
        if (!CheckSqliteError(sqlite3_exec(database, sql_string.c_str(), nullptr, nullptr, &errmsg), database)){
            std::cout << errmsg << std::endl;
            std::cout << sql_string << std::endl;
        }
    }
        
    Settings PiranhaData::GetSettings(){
        Settings settings;
        std::string sql = "SELECT count(1) FROM Settings";
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare_v2(database, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database)){
            std::cout << "Error with: " << sql << std::endl;
            return settings;
        }
        int step = 0;
        int count = 0;
        while ((step = sqlite3_step(statement)) == SQLITE_ROW){
            count = sqlite3_column_int(statement, 0);
        };
        sqlite3_finalize(statement);
        if (count == 0){
            UpdateSettings(settings);
        } else {
            sql = "SELECT LatestUpdateTimeStamp, Language FROM Settings";
            if (!CheckSqliteError(sqlite3_prepare_v2(database, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database)){
                std::cout << "Error with: " << sql << std::endl;
                return settings;
            }
            std::string timestamp, language;
            while ((step = sqlite3_step(statement)) == SQLITE_ROW){
                const unsigned char *str_timestamp = sqlite3_column_text(statement, 0);
                if (str_timestamp != nullptr){
                    timestamp = std::string(reinterpret_cast<const char *>(str_timestamp));
                }
                
                const unsigned char *str_language = sqlite3_column_text(statement, 1);
                if (str_language != nullptr){
                    language = std::string(reinterpret_cast<const char *>(str_language));
                }
            };
            sqlite3_finalize(statement);
            settings = Settings(timestamp, language);
            return settings;
        }
        return settings;
    }
    
    void PiranhaData::UpdateSettings(Settings settings){
        std::string sql = "SELECT count(1) FROM Settings";
        sqlite3_stmt *statement;
        int step = 0;
        int count = 0;
        if (!CheckSqliteError(sqlite3_prepare_v2(database, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database)){
            std::cout << "Error with: " << sql << std::endl;
        }
        while ((step = sqlite3_step(statement)) == SQLITE_ROW){
            count = sqlite3_column_int(statement, 0);
        };
        sqlite3_finalize(statement);
        
        std::string sql_query;
        
        if (count == 0){
            sql_query = "INSERT INTO Settings (LatestUpdateTimeStamp, Language) VALUES ('" + settings.latest_update_timestamp() + "', '" + settings.language() + "')";
        } else {
            sql_query = "UPDATE Settings SET LatestUpdateTimeStamp='" + settings.latest_update_timestamp() + "', Language='" + settings.language() + "'";
        }
        
        ExecuteSqlQuery(sql_query);
    }
    
    void PiranhaData::AddOrUpdatePage(Page page){
        char order_id[256];
        sprintf(order_id, "%d", page.order_id());
        
        std::string update_page_string = "INSERT OR REPLACE INTO Page ('Id', 'Parentid', 'Title', 'Regions', 'Sync', 'OrderId', 'TemplateName') VALUES ('" + page.guid() + "', '" + page.parent_id() + "', '" + escapeSQLString(page.title()) + "', '" + escapeSQLString(page.regions()) + "', '" + (page.sync() ? "1" : "0") + "', '" + std::string(order_id) + "', '" + escapeSQLString(page.template_name()) + "')";
        ExecuteSqlQuery(update_page_string);
    }
    
    void PiranhaData::RemovePage(std::string page_id){
        std::string sql_string = "DELETE FROM Page WHERE Id='" + page_id + "'";
        ExecuteSqlQuery(sql_string);
    }
    
    void PiranhaData::PageFromStatement(Page &page, sqlite3_stmt *statement){
        std::string page_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
        std::string parent_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 1)));
        std::string title(reinterpret_cast<const char *>(sqlite3_column_text(statement, 2)));
        std::string regions(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
        bool sync = sqlite3_column_int(statement, 4);
        int order_id = sqlite3_column_int(statement, 5);
        std::string template_name(reinterpret_cast<const char *>(sqlite3_column_text(statement, 6)));
        
        page.title(title);
        page.guid(page_id);
        page.parent_id(parent_id);
        page.regions(regions);
        page.sync(sync);
        page.order_id(order_id);
        page.template_name(template_name);
    }
    
    Page PiranhaData::PageFromSQLString(std::string sql_string){
        Page page;
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length()+1, &statement, nullptr), database)){
            sqlite3_finalize(statement);
            return Page();
        }
        
        while (sqlite3_step(statement) == SQLITE_ROW){
            PageFromStatement(page, statement);
        }
        
        sqlite3_finalize(statement);
        return page;
    }
    // int GetCount()
    std::vector<Page> PiranhaData::PagesFromSQLString(std::string sql_string){
        std::vector<Page> pages;
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length()+1, &statement, nullptr), database)){
            sqlite3_finalize(statement);
            return pages;
        }
        
        while (sqlite3_step(statement) == SQLITE_ROW){
            Page page;
            PageFromStatement(page, statement);
            pages.push_back(page);
        }
        
        sqlite3_finalize(statement);
        return pages;
    }
    
    Page PiranhaData::GetPageWithId(std::string page_id){
        std::string sql_string = "SELECT Id, ParentId, Title, Regions, Sync, OrderId, TemplateName FROM Page WHERE Id='" + page_id + "' ORDER BY OrderId ASC";
        return PageFromSQLString(sql_string);
    }
    
    void PiranhaData::AddContent(Content content){
        char type_buffer[1024];
        sprintf(type_buffer, "%d", (int)content.type());
        
        std::string sql_string = "INSERT OR REPLACE INTO Content ('Id', 'MimeType', 'FileName', 'ContentURL', 'ThumbnailURL', 'DisplayName', 'Description', 'Downloaded', 'Type', 'Updated', 'Ignore') VALUES ('" +
        content.content_guid() + "', '" +
        content.mime_type() + "', '" +
        escapeSQLString(content.file_name()) + "', '" +
        content.content_url() + "', '" +
        content.thumbnail_url() + "', '" +
        escapeSQLString(content.display_name()) + "', '" +
        escapeSQLString(content.description()) + "', '" +
        (content.is_downloaded() ? "1" : "0") + "', '" +
        std::string(type_buffer) + "', '" +
        content.updated() + "', '" +
        (content.ignore() ? "1" : "0")+ "')";
        ExecuteSqlQuery(sql_string);
    }
    
    void PiranhaData::RemoveContent(std::string content_id){
        std::string sql_string = "DELETE FROM Content WHERE Id='" + content_id + "'";
        ExecuteSqlQuery(sql_string);
    }
    
    void PiranhaData::AddPageContent(std::string page_id, std::string content_id){
        std::string sql_string = "INSERT OR REPLACE INTO PageContent ('PageId', 'ContentId') VALUES ('" + page_id + "', '" + content_id + "')";
        ExecuteSqlQuery(sql_string);
    }
    
    void PiranhaData::RemovePageContentForPage(std::string page_id){
        std::string sql_string = "DELETE FROM PageContent WHERE PageId='" + page_id + "'";
        ExecuteSqlQuery(sql_string);
    }
    
    void PiranhaData::RemovePageContentForContent(std::string content_id){
        std::string sql_string = "DELETE FROM PageContent WHERE ContentId='" + content_id + "'";
        ExecuteSqlQuery(sql_string);
    }
    
    Content ContentFromStatement(sqlite3_stmt *statement){
        Content content;
        
        std::string content_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
        std::string mime_type(reinterpret_cast<const char *>(sqlite3_column_text(statement, 1)));
        std::string filename(reinterpret_cast<const char *>(sqlite3_column_text(statement, 2)));
        std::string content_url(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
        std::string thumbnail_url(reinterpret_cast<const char *>(sqlite3_column_text(statement, 4)));
        std::string display_name(reinterpret_cast<const char *>(sqlite3_column_text(statement, 5)));
        std::string description(reinterpret_cast<const char *>(sqlite3_column_text(statement, 6)));
        bool is_downloaded = sqlite3_column_int(statement, 7);
        int type = sqlite3_column_int(statement, 8);
        std::string updated(reinterpret_cast<const char *>(sqlite3_column_text(statement, 9)));
        bool ignore = sqlite3_column_int(statement, 10);
        
        content.content_guid(content_id);
        content.mime_type(mime_type);
        content.file_name(filename);
        content.content_url(content_url);
        content.thumbnail_url(thumbnail_url);
        content.display_name(display_name);
        content.description(description);
        content.is_downloaded(is_downloaded);
        content.type(static_cast<ContentType>(type));
        content.updated(updated);
        content.ignore(ignore);
        return content;
    }
    
    Content PiranhaData::ContentFromSQLString(std::string sql_string){
        auto contents = ContentsFromSQLString(sql_string);
        if (contents.size() > 0){
            return contents[0];
        }
        return Content();
    }
    
    std::vector<Content> PiranhaData::ContentsFromSQLString(std::string sql_string){
        std::vector<Content> contents;
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, nullptr), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        
        while (sqlite3_step(statement) == SQLITE_ROW){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        }
        
        sqlite3_finalize(statement);
        return contents;
    }
    
    Content PiranhaData::GetContentWithId(std::string content_id){
        std::string sql_string("SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, Downloaded, Type, Updated FROM Content WHERE Id='" + content_id + "'");
        return ContentFromSQLString(sql_string);
    }
    
    std::vector<Content> PiranhaData::GetContentsForType(ContentType content_type){
        
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, Downloaded, Type, Updated FROM Content WHERE Type=" + Utilities::NumberToString(content_type);
        sqlite3_stmt *statement;
        std::vector<Content> contents;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, NULL), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        while (sqlite3_step(statement) == SQLITE_ROW) {
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        }
        sqlite3_finalize(statement);
        return contents;
    }
    
    std::vector<std::string> PiranhaData::ContentIdsForPage(std::string page_id){
        std::string sql_string("SELECT ContentId FROM PageContent WHERE PageId='" + page_id + "'");
        sqlite3_stmt *statement;
        std::vector<std::string> contents;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, NULL), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        while (sqlite3_step(statement) == SQLITE_ROW) {
            std::string content_id(reinterpret_cast<const char*>(sqlite3_column_text(statement, 0)));
            contents.push_back(content_id);
        }
        sqlite3_finalize(statement);
        return contents;
    }
    
    std::list<std::string> PiranhaData::AllContentIds(){
        std::string sql_string("SELECT Id FROM Content");
        sqlite3_stmt *statement;
        std::list<std::string> contents;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, NULL), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        while (sqlite3_step(statement) == SQLITE_ROW) {
            std::string content_id(reinterpret_cast<const char*>(sqlite3_column_text(statement, 0)));
            contents.push_back(content_id);
        }
        sqlite3_finalize(statement);
        return contents;
    }
    
    std::vector<Page> PiranhaData::GetChildPagesForPageWithId(std::string page_id){
        std::string sql_string = "SELECT * FROM Page WHERE Parentid = '" + page_id + "' ORDER BY OrderId ASC";
        return PagesFromSQLString(sql_string);
    }
    
    std::vector<Page> PiranhaData::GetPagesWithTemplate(std::string template_name){
        std::string sql_string = "SELECT * FROM Page WHERE TemplateName = '" + template_name + "'";
        return PagesFromSQLString(sql_string);
    }
    
    std::vector<Content> PiranhaData::UnfinishedContent(){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, Downloaded, Type, Updated, Ignore FROM PageContent JOIN Content ON Id=ContentId WHERE Downloaded=0 AND Ignore=0 GROUP BY Id";
        std::vector<Content> contents = ContentsFromSQLString(sql_string);
        std::string sql_string_images = "SELECT * FROM Content WHERE MimeType LIKE '%image%' AND Downloaded=0 AND Ignore=0";
        std::vector<Content> images = ContentsFromSQLString(sql_string_images);
        contents.insert(contents.end(), images.begin(), images.end());
        
        return contents;
    }
    
    void PiranhaData::SetAllContentToIgnore(bool ignore){
        char buffer[512];
        sprintf(buffer, "UPDATE Content SET Ignore = %d", ignore);
        std::string sql(buffer);
        ExecuteSqlQuery(sql);
    }
    
    std::vector<Content> PiranhaData::GetContentForPageWithId(std::string page_id, ContentType type){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, Downloaded, ContentId, Updated FROM PageContent pc INNER JOIN Content c ON ContentId = Id WHERE PageId='" + page_id + "' and Type=" + Utilities::NumberToString(type);
        std::vector<Content> contents;
        
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, NULL), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        while (sqlite3_step(statement) == SQLITE_ROW){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        }
        sqlite3_finalize(statement);
        
        return contents;
    }
    
    std::vector<Content> PiranhaData::GetContentForPageWithId(std::string page_id){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, Downloaded, ContentId, Updated FROM PageContent pc INNER JOIN Content c ON ContentId = Id WHERE PageId='" + page_id + "'";
        std::vector<Content> contents;
        
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare(database, sql_string.c_str(), (int)sql_string.length() + 1, &statement, NULL), database)){
            sqlite3_finalize(statement);
            return contents;
        }
        while (sqlite3_step(statement) == SQLITE_ROW){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        }
        sqlite3_finalize(statement);
        
        return contents;
    }
    
    void PiranhaData::ClearPages(){
        std::string drop_pages = "DROP TABLE Page";
        std::string drop_page_content = "DROP TABLE PageContent";
        ExecuteSqlQuery(drop_page_content);
        ExecuteSqlQuery(drop_pages);
    }
    
    void PiranhaData::ResetTables(){
        std::string drop_pages        = "DROP TABLE Page";
        std::string drop_page_content = "DROP TABLE PageContent";
        std::string drop_content      = "DROP TABLE Content";

        ExecuteSqlQuery(drop_pages);
        ExecuteSqlQuery(drop_page_content);
        ExecuteSqlQuery(drop_content);
        
        CreateTables();
    }
}