//
//  ThreadPool.h
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _ThreadPool_H_
#define _ThreadPool_H_

#include <thread>
#include <vector>
#include <deque>
#include <condition_variable>
namespace tin{
    class ThreadPool;
    class Worker{
        public:
            Worker(ThreadPool &pool) : pool_(pool) { };
            void operator()();
        
        private:
            ThreadPool &pool_;
    };
    
    class ThreadPool {
        public:
            ThreadPool(size_t size);
            ~ThreadPool();
        
            template<class F, class Fobj, class... Args>
            void enqueue(F &&f, Fobj &&o, Args&&... args){
                {
                    std::unique_lock<std::mutex> lock(queue_mutex_);
                    std::function<void()> func = std::bind(std::forward<F>(f), std::forward<Fobj>(o), std::forward<Args>(args)...);
                    tasks_.push_back(func);
                }
                condition_variable_.notify_all();
            }
        
            void WaitUntilEmpty();
        private:
            friend class Worker;
        
            std::vector<std::thread> workers_;
            std::deque<std::function<void()>> tasks_;
        
            std::mutex queue_mutex_;
            std::condition_variable condition_variable_;
        
            bool stop_;
    };
} // tin
#endif
