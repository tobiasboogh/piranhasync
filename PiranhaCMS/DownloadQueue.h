//
//  DownloadQueue.h
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/25/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _DownloadQueue_H_
#define _DownloadQueue_H_

#include <list>
#include <mutex>
#include <string>
#include <thread>
#include <functional>
#include <condition_variable>

namespace tin{
    typedef std::function<void(bool success, std::string url, std::string filename, std::string guid, std::string string_data)> DownloadQueueCompletionCallback;
    typedef std::function<void(int downloads_left)> ProgressCallback;
    
    typedef struct Download {
        Download(std::string url, std::string filename, std::string guid, bool return_data, DownloadQueueCompletionCallback callback) : url(url), filename(filename), callback(callback), return_data(return_data), guid(guid){ };
        std::string url;
        std::string filename;
        std::string guid;
        DownloadQueueCompletionCallback callback;
        bool return_data;
    } Download;
    
    /**
     Extern functions required to be handled by host system.
     @param url The url to download from
     @param return_data If the data downloaded should be passed to the callback
     @param file_name If return_data is false the specify the file name to download to
     @param callback The completion callback to be called when the download is done.
     */
    extern void DownloadFileFromURL(std::string url, bool return_data, std::string file_name, std::string guid,DownloadQueueCompletionCallback callback);
    
    class DownloadQueue {
        public:
            DownloadQueue(int max_connections);
            ~DownloadQueue();
            void AddDownload(std::string url, std::string filename, std::string guid, bool return_data, DownloadQueueCompletionCallback callback);
            void DownloadCompleted(std::string url);
            void AddProgressCallback(ProgressCallback progress_callback);
            void RemoveProgressCallback();
        private:
            std::list<Download>     download_queue_;
            std::list<Download>     current_downloads_;
        
            std::thread             download_thread_;
            std::mutex              list_mutex_;
            std::condition_variable wait_cond_var_;
            std::mutex              wait_mutex_;

            bool                    running_;
            int                     max_connections_;
        
            ProgressCallback        progress_callback_;
        
            int DownloadQueueSize();
            int CurrentDownloadsSize();
        
            void ThreadLoop();
    };
    
} // tin
#endif
