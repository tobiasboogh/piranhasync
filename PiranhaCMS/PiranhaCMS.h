//
//  PiranhaCMS.h
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#ifndef _PiranhaCMS_H_
#define _PiranhaCMS_H_

#include "Singleton.h"
#include <list>
#include <string>
#include <vector>
#include <functional>
#include <json/json.h>

#include "Delegate.h"
#include "ThreadPool.h"
//#include "DownloadQueue.h"

#include "Database.h"
#include "Repositories/PostRepository.h"
#include "Repositories/PageRepository.h"
#include "Repositories/ContentRepository.h"
#include "Repositories/SettingsRepository.h"

#define EMPTY_GUID "00000000-0000-0000-0000-000000000000"


namespace tin{
    
    typedef std::function<void(bool success)> SimpleCompletionCallback;
    typedef std::function<void(bool updates_available)> UpdateAvailableCallback;
    typedef std::function<void(bool success, std::string url, std::string filename, std::string guid, std::string string_data)> DownloadQueueCompletionCallback;
    typedef std::function<void(int downloads_left)> ProgressCallback;
    
    typedef struct PiranhaCMSOptions{
        std::string         site_url;
        std::string         base_path;
        std::string         database_path;
        int                 max_connections;
        Delegate           *delegate;
    } PiranhaCMSOptions;
    
    
    extern void DeleteFile(std::string file_path);
    
    class PiranhaCMS : public Singleton<PiranhaCMS>{
        public:
            std::string    GetVersion();
        
            /**
                Must be called after creation to set the site url and number of connections to use
                @param options The options structure with desired values
             */
            void    SetOptions(PiranhaCMSOptions options);
        
            /**
                Return true if any changes are available on the server
                @return True if any changesa are available
             */
            void    ChangesAvailableAsync(UpdateAvailableCallback callback);
        
            /**
                Gets latest changes async
                 @param callback The succes callback called on completion with the result of the operation
             */
        
            void    GetLatestChangesAsync(DownloadQueueCompletionCallback callback);
        
            /**
                Converts a time since epoch value to a Piranha formatted string
                @param timestamp Time since epoch
                @return String representing the time
             */
            static std::string TimestampToServerDateString(int64_t timestamp);
        
            /**
                Converts a server date string to time since epoch
                @param date The server date
                @return Time since epoch
             */
            static int64_t     ServerDateStringToTimestamp(std::string date);
        
            /**
                Downloads the given content from the server
                @param content The content
             */
            void    DownloadContent(Content content);
            void    DownloadContentWithId(std::string content_id);
            /**
                Downloads all content associated with the given page
                @param page The page
             */
            void    DownloadContentForPage(Page &page);
        
            bool    DownloadAllUnfinishedContent();
        
//            DownloadQueue* download_queue() { return download_queue_; };
        
            void    ResetContentForNewLanguage(std::string language);
            void    ClearContentForTypes(int types);
        
            template<typename T>
            void SetDelegate(){
                T* delegate = new T();
                delegate_ = delegate;
            }
        
            Delegate *delegate() { return delegate_; }
            std::string SiteURL(){ return site_url_; };
        
            PostRepository      *post_repository() {return post_repository_;};
            PageRepository      *page_repository() {return page_repository_;};
            ContentRepository   *content_repository() {return content_repository_; };
            SettingsRepository  *settings_repository(){return settings_repository_;};
        private:
            void    Init();
            void    Shutdown();
        
            Database                        *database_;
            PostRepository                  *post_repository_;
            PageRepository                  *page_repository_;
            ContentRepository               *content_repository_;
            SettingsRepository              *settings_repository_;
        
//            PiranhaData*                    piranha_data_;
        
            std::string                     site_url_;
            std::string                     base_path_;
        
//            DownloadQueue*                  download_queue_;
        
            Delegate*                       delegate_;
        
            void HandlePagesAsync(Json::Value &pages);
            void UpdatePage(Json::Value &json_page, tin::Page &page);
        
            void HandlePostsAsync(Json::Value &posts);
            void UpdatePost(Json::Value &json_page, tin::Post &post);
        
            void HandleContentsAsync(Json::Value &contents);
            void UpdateContent(Json::Value &json_content, tin::Content &content);
        
        
            void HandleSitemapNode(Json::Value &sitemap_node, std::string parent_id);
        
            void HandleDeletions(Json::Value &deletions, std::string base_path);
            void HandleSitemap(Json::Value &sitemap_root);
        
    
            friend class Singleton;
    };
    
//    extern void AddDownload(std::string url, std::string filename, std::string guid, bool return_data, DownloadQueueCompletionCallback callback);
    extern void DownloadFileFromURL(std::string url, bool return_data, std::string file_name, std::string guid, DownloadQueueCompletionCallback callback);
} // tin
#endif
