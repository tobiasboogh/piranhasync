//
//  DownloadQueue.cpp
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/25/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "DownloadQueue.h"
#include <iostream>

#define ANDROIDPRINT 0
#if ANDROIDPRINT
#include <android/log.h>
#endif
namespace tin {
    DownloadQueue::DownloadQueue(int max_connections){
        max_connections_ = max_connections;
        download_thread_ = std::thread(&DownloadQueue::ThreadLoop, this);
    }
    
    DownloadQueue::~DownloadQueue(){
        running_ = false;
        wait_cond_var_.notify_all();
        download_thread_.join();
    }
    
    std::mutex print_mutex_;
    void DebugPrint(std::string message){
        std::unique_lock<std::mutex> lock(print_mutex_);
#if ANDROIDPRINT
//        __android_log_print(ANDROID_LOG_INFO, "tin::Info", message.c_str());
#else
      std::cout << message << std::endl;
#endif
      
    }
    
    void DownloadQueue::AddDownload(std::string url, std::string filename, std::string guid, bool return_data, DownloadQueueCompletionCallback callback){
        list_mutex_.lock();
        for (auto listed_download : download_queue_){
            if (listed_download.url == url){
                std::cout << "Download already exists " << url << std::endl;
                list_mutex_.unlock();
                return;
            }
        }
        Download download(url, filename, guid, return_data, callback);
        download_queue_.push_back(download);
        wait_cond_var_.notify_all();
        list_mutex_.unlock();
    }
    
    void DownloadQueue::ThreadLoop(){
        running_ = true;
        std::cout << "DownloadQueue::ThreadLoop()" << std::endl;
        do {
            int current_size = CurrentDownloadsSize();
            if (DownloadQueueSize() == 0){
                std::unique_lock<std::mutex> lock(wait_mutex_);
                while (DownloadQueueSize() == 0 && running_){
                    wait_cond_var_.wait(lock);
                }
            }
            if (DownloadQueueSize() > 0 && current_size < max_connections_ && running_){
                list_mutex_.lock();
                Download download = download_queue_.front();
                download_queue_.pop_front();
                current_downloads_.push_back(download);
                list_mutex_.unlock();
                
                
                DownloadFileFromURL(download.url, download.return_data, download.filename, download.guid, [download, this](bool success, std::string url, std::string filename, std::string guid, std::string string_data){
                    for (auto iter = current_downloads_.begin(); iter != current_downloads_.end(); ++iter){
                        if (iter->url == download.url){
                            list_mutex_.lock();
                            current_downloads_.erase(iter);
                            list_mutex_.unlock();
                            break;
                        }
                    }
                    if (download.return_data){
                        download.callback(success, url, filename, guid, string_data);
                    } else {
                        download.callback(success, url, filename, "", "");
                    }
                    if (progress_callback_ != nullptr){
                        progress_callback_(this->CurrentDownloadsSize() + this->DownloadQueueSize());
                    }
                });
            }
        } while (running_);
        std::cout << "DownloadQueue::ThreadLoop() END" << std::endl;
    }
    
    void DownloadQueue::AddProgressCallback(ProgressCallback progress_callback){
        progress_callback_ = progress_callback;
    }
    
    void DownloadQueue::RemoveProgressCallback(){
        progress_callback_ = nullptr;
    }
    
    int DownloadQueue::DownloadQueueSize(){
        int size = 0;
        list_mutex_.lock();
        size = static_cast<int>(download_queue_.size());
        list_mutex_.unlock();
        return size;
    }
    
    int DownloadQueue::CurrentDownloadsSize(){
        int size = 0;
        list_mutex_.lock();
        size = static_cast<int>(current_downloads_.size());
        list_mutex_.unlock();
        return size;
    }
}