//
//  PiranhaCMS.cpp
//  PiranhaTests
//
//  Created by Tobias Boogh on 6/3/13.
//  Copyright (c) 2013 Tobias Boogh. All rights reserved.
//

#include "PiranhaCMS.h"
#include "PiranhaClasses.h"
#include "Models/Settings.h"
#include "Models/Page.h"

#include "Utilities.h"
#include <iostream>
#include <fstream>
#include <chrono>
#include <time.h>
#include <assert.h>
#include <json/json.h>
#define THREADED 1

#if ANDROID

#include <android/log.h>

#define DEBUG_TAG "tin::Piranha"
#define LOGINFO(fmt, ...) __android_log_print(ANDROID_LOG_INFO, DEBUG_TAG, fmt, __VA_ARGS__)
#define LOGERROR(fmt, ...) __android_log_print(ANDROID_LOG_ERROR, DEBUG_TAG, fmt, __VA_ARGS__)

#endif

namespace tin {

    static ThreadPool* thread_pool_;
    
    const std::string               title_identifier = "Title";
    const std::string               id_identifier = "Id";
    const std::string               has_children_identifier = "HasChildren";
    const std::string               child_nodes_identifier = "ChildNodes";
    
    int                             page_sort_order;
    
    void PiranhaCMS::Init(){
#if THREADED
        thread_pool_ =  new ThreadPool(2);
#endif
//        piranha_data_ = PiranhaData::SharedInstance();
//        download_queue_ = new DownloadQueue(4);
    }
    
    std::string PiranhaCMS::GetVersion(){
        return "1.2";
    }
    
    void PiranhaCMS::SetOptions(PiranhaCMSOptions options){
        site_url_ = options.site_url;
        base_path_ = options.base_path;
        delegate_ = options.delegate;
        database_ = new Database(options.database_path);
        if (!database_){
            std::cout << "Error creating database" << std::endl;
            return;
        } else {
            post_repository_ = new PostRepository(database_, this);
            page_repository_ = new PageRepository(database_, this);
            settings_repository_ = new SettingsRepository(database_, this);
            content_repository_ = new ContentRepository(database_, this);

            post_repository_->CreateTable();
            page_repository_->CreateTable();
            settings_repository_->CreateTable();
            content_repository_->CreateTable();
        }
    }
    
    void PiranhaCMS::Shutdown(){
        delete delegate_;
//        delete download_queue_;
        delete thread_pool_;
    }

    std::string FixTimestamp(std::string timestamp){
        timestamp = Utilities::StringByReplacing(timestamp, '/', "-");
        timestamp = Utilities::StringByReplacing(timestamp, ':', "-");
        timestamp = Utilities::StringByReplacing(timestamp, ' ', "/");
        // Need to fix the ordering of the date
        std::string year = timestamp.substr(0, 4);
        std::string month = timestamp.substr(5, 2);
        std::string day = timestamp.substr(8, 2);
        
        size_t slash_pos = timestamp.find("/");
        std::string time = timestamp.substr(slash_pos, timestamp.length() - slash_pos);
        
        std::string fixed_timestamp = year + "-" + month + "-" + day + time;
        return fixed_timestamp;
    }

    bool IsTimestampValid(std::string timestamp){
        if (timestamp.substr(4,1) != "-" ||
            timestamp.substr(7, 1) != "-" ||
            timestamp.substr(10, 1) != "/" ||
            timestamp.substr(13, 1) != "-" ||
            timestamp.substr(16, 1) != "-"){
            return false;
        }
        return true;
    }
    
    void PiranhaCMS::GetLatestChangesAsync(DownloadQueueCompletionCallback callback){
        page_sort_order = 0;
        std::string url_timestamp = "1970-01-01/01-01-01";
        Settings settings = settings_repository_->GetSettings();
        if (IsTimestampValid(settings.latest_update_timestamp())) {
            url_timestamp = settings.latest_update_timestamp();
        }
        std::string url = site_url_ + "/rest/changes/get/" + settings.language() + "/" + url_timestamp;
        std::cout << url << std::endl;
        
        DownloadFileFromURL(url, true, "", "", [this, callback](bool success, std::string url, std::string filename, std::string guid, std::string string_data){
            if (string_data != ""){
                Json::Value changes_root;
                Json::Reader reader;
                success = reader.parse(string_data, changes_root);
                if (!success){
                    std::cout << "PARSE FAILURE" << std::endl;
                    std::cout << string_data << std::endl;
                    callback(success, url, filename, "", "");
                    return;
                }
                
                Json::Value sitemap_root = changes_root.get("Sitemap", Json::Value());
                HandleSitemap(sitemap_root);
                if (delegate_ != nullptr){
                    delegate_->OnSitemapProcessed(this);
                }
                
                Json::Value posts = changes_root.get("Posts", Json::Value());
                HandlePostsAsync(posts);
                
                Json::Value contents = changes_root.get("Content", Json::Value());
                HandleContentsAsync(contents);
                
                Json::Value pages = changes_root.get("Pages", Json::Value());
                HandlePagesAsync(pages);
                
                
                
    #if THREADED
                thread_pool_->WaitUntilEmpty();
    #endif
                if (delegate_ != nullptr){
                    delegate_->OnChangesProcessed(this);
                }
                
                Json::Value deleted = changes_root.get("Deleted", Json::Value());
                HandleDeletions(deleted, base_path_);
                
                std::string timestamp = changes_root.get("Timestamp", "").asString();
                timestamp = FixTimestamp(timestamp);
                if (!IsTimestampValid(timestamp)){
                    std::cout << "INVALID: " << timestamp << std::endl;
                    timestamp = "1970-01-01/00-00-00";
                }
                Settings settings = settings_repository_->GetSettings();
                settings.latest_update_timestamp(timestamp);
                settings_repository_->UpdateSettings(settings);
                
                callback(success, url, filename, "", string_data);
            }
        });
    }
    
    bool IsArraySizeGreaterThanZero(Json::Value parent, std::string node_name){
        Json::Value child = parent.get(node_name, Json::Value());
        return (child.size() > 0);
    }
    
    void PiranhaCMS::ChangesAvailableAsync(UpdateAvailableCallback callback){

        Settings settings = settings_repository_->GetSettings();
        std::string url_timestamp = "1970-01-01/00-00-00";
        if (IsTimestampValid(settings.latest_update_timestamp())) {
            url_timestamp = settings.latest_update_timestamp();
        }

        std::string url = site_url_ + "/rest/changes/get/" + settings.language() + "/" + url_timestamp;
        
        std::cout << url << std::endl;
        
        DownloadFileFromURL(url, true, "", "", [callback](bool success, std::string url, std::string filename, std::string guid, std::string string_data){
            Json::Value changes_root;
            Json::Reader reader;
            success = reader.parse(string_data, changes_root);
            
            if (!success){
                std::cout << "PARSE FAILURE" << std::endl;
                std::cout << string_data << std::endl;
                callback(false);
            }
            
            Json::Value sitemap_root = changes_root.get("Sitemap", Json::Value());
            if (sitemap_root.size() > 0){
                callback(true);
                return;
            }
            
            Json::Value pages = changes_root.get("Pages", Json::Value());
            if (pages.size() > 0){
                callback(true);
                return;
            }
            
            Json::Value posts = changes_root.get("Posts", Json::Value());
            if (posts.size() > 0){
                callback(true);
                return;
            }
            
            Json::Value contents = changes_root.get("Content", Json::Value());
            if (contents.size() > 0){
                callback(true);
                return;
            }
            Json::Value deleted = changes_root.get("Deleted", Json::Value());
            
            if (IsArraySizeGreaterThanZero(deleted, "Pages")){
                callback(true);
                return;
            }
            
            if (IsArraySizeGreaterThanZero(deleted, "Posts")){
                callback(true);
                return;
            }
            
            if (IsArraySizeGreaterThanZero(deleted, "Content")){
                callback(true);
                return;
            }
            
            if (callback != nullptr){
                callback(false);
            }
        });
    }
        
    void PiranhaCMS::HandleSitemap(Json::Value &sitemap_root){
        for (int i=0; i < sitemap_root.size(); i++){
            Json::Value value = sitemap_root.get(i, Json::Value());
            HandleSitemapNode(value, "");
#if THREADED
            
            thread_pool_->WaitUntilEmpty();
#endif
        }
    }
    
    void PiranhaCMS::UpdatePage(Json::Value &json_page, tin::Page &page){
        std::string title = json_page.get(title_identifier, "").asString();
        std::string page_id = json_page.get(id_identifier, "").asString();
        page.title(title);
        page.guid(page_id);
        
        page.ClearAttachments();
        
        content_repository_->RemoveRelationshipForGuid(page_id);
        Json::Value attachments = json_page.get("Attachments", Json::Value());
        for (int i=0; i < attachments.size(); i++){
            Json::Value attachment = attachments[i];
            std::string attachment_id = attachment.get("Id", "").asString();
            page.AddAttachment(attachment_id);
            content_repository_->AddRelationshipForGuid(page.guid(), attachment_id);
        }
        Json::StyledWriter writer;
        Json::Value regions = json_page.get("Regions", Json::Value());
        std::string regions_string = writer.write(regions);
        page.regions(regions_string);
        
        std::string template_name = json_page.get("TemplateName", "").asString();
        page.template_name(template_name);
        
        Json::Value extensions = json_page.get("Extensions", Json::Value());
        std::string extensions_string = writer.write(extensions);
        page.extensions(extensions_string);
        
        bool is_hidden = json_page.get("IsHidden", false).asBool();
        page.is_hidden(is_hidden);
        
        int seq_no = json_page.get("Seqno", -1).asInt();
        // Check to see if RestAPI version supports seqno
        if (seq_no != -1){
            page.order_id(seq_no);
        }
        
        page_repository_->Replace(page);
        
        if (page.sync()){
            page_repository_->SetSyncForPageWithId(page_id, true, true);
        }
        if (delegate_ != nullptr){
            if (delegate_->OnPageProcessedNotification()){
                delegate_->OnPageProcessed(this, page);
            }
        }
    }
    
    void PiranhaCMS::HandlePagesAsync(Json::Value &pages){
        for (int i=0; i < pages.size(); i++){
            Json::Value json_page = pages[i];
            std::string page_id = json_page.get("Id", "").asString();

            Page page = page_repository_->GetPageWithId(page_id);
            if (page != Page()){
                thread_pool_->enqueue(&PiranhaCMS::UpdatePage, this, json_page, page);
            } else {
                std::cout << "Missing page: " << page_id << std::endl;
            }
        }
    }
    
    void PiranhaCMS::UpdatePost(Json::Value &json, tin::Post &post){
        std::string title = json.get(title_identifier, "").asString();
        std::string guid = json.get(id_identifier, "").asString();
        post.title(title);
        post.guid(guid);
        
        post.ClearAttachments();
        
        content_repository_->RemoveRelationshipForGuid(guid);
        Json::Value attachments = json.get("Attachments", Json::Value());
        for (int i=0; i < attachments.size(); i++){
            Json::Value attachment = attachments[i];
            std::string attachment_id = attachment.get("Id", "").asString();
            post.AddAttachment(attachment_id);
            content_repository_->AddRelationshipForGuid(post.guid(), attachment_id);
        }
        Json::StyledWriter writer;
        Json::Value regions = json.get("Regions", Json::Value());
        std::string regions_string = writer.write(regions);
        post.regions(regions_string);
        
        std::string template_name = json.get("TemplateName", "").asString();
        post.template_name(template_name);
        
        Json::Value extensions = json.get("Extensions", Json::Value());
        std::string extensions_string = writer.write(extensions);
        post.extensions(extensions_string);
        
        post_repository()->Replace(post);
        if (delegate_ != nullptr){
            if (delegate_->OnPostProcessedNotification()){
                delegate_->OnPostProcessed(this, post);
            }
        }
    }
    
    void PiranhaCMS::HandlePostsAsync(Json::Value &posts){
        for (int i=0; i < posts.size(); i++){
            Json::Value json = posts[i];
            std::string guid = json.get("Id", "").asString();
            Post post = post_repository()->GetWithId(guid);
            thread_pool_->enqueue(&PiranhaCMS::UpdatePost, this, json, post);
        }
    }
    
    void PiranhaCMS::HandleSitemapNode(Json::Value &sitemap_node, std::string parent_id){
        Page page;
        std::string title = sitemap_node.get(title_identifier, "").asString();
        std::string page_id = sitemap_node.get(id_identifier, "").asString();
        page = page_repository_->GetPageWithId(page_id);
        
        // This is a new page
        if (page.guid() == ""){
            Page parent_page = page_repository()->GetPageWithId(parent_id);
            page.sync(parent_page.sync());
        }
        page.title(title);
        page.guid(page_id);
        page.parent_id(parent_id);
        page.order_id(page_sort_order++);
        bool has_children = sitemap_node.get(has_children_identifier, false).asBool();
        if (has_children){
            Json::Value child_array = sitemap_node.get(child_nodes_identifier, Json::Value());
            for (int i=0; i < child_array.size(); i++){
                Json::Value child = child_array[i];
                
                HandleSitemapNode(child, page_id);
            }
        }
        page_repository_->Replace(page);
    }
    
    void PiranhaCMS::HandleContentsAsync(Json::Value &contents){
        for (int i=0; i < contents.size(); i++){
            Json::Value json_content = contents[i];
            std::string content_id = json_content.get("Id", "").asString();
            if (content_id == "03a93bb2-01c8-4f8f-9b94-fcc73ca3df6f"){
                std::cout << "03a93bb2-01c8-4f8f-9b94-fcc73ca3df6f HandleContent" << std::endl;
            }
            Content content;
            content = content_repository_->GetContentWithId(content_id);

            thread_pool_->enqueue(&PiranhaCMS::UpdateContent, this, json_content, content);
        }
    }
    
    void PiranhaCMS::UpdateContent(Json::Value &json_content, tin::Content &content){
        std::string test_id = json_content.get("Id", "").asString();
        if (test_id == "03a93bb2-01c8-4f8f-9b94-fcc73ca3df6f"){
            std::cout << "03a93bb2-01c8-4f8f-9b94-fcc73ca3df6f UpdateContent" << std::endl;
        }
        std::string updated = json_content.get("Updated", "").asString();
        if (content.updated() == updated){
            return;
        }
        std::string content_id = json_content.get("Id", "").asString();
        std::string file_name = json_content.get("Filename", "").asString();
        std::string content_url = json_content.get("ContentUrl", "").asString();
        std::string thumbnail_url = json_content.get("ThumbnailUrl", "").asString();
        std::string description = json_content.get("Description", "").asString();
        std::string display_name = json_content.get("DisplayName", "").asString();
        std::string mime_type = json_content.get("Type", "").asString();

        content.guid(content_id);
        content.file_name(file_name);
        content.content_url(content_url);
        content.thumbnail_url(thumbnail_url);
        content.description(description);
        content.display_name(display_name);
        content.mime_type(mime_type);
        // Downloaded and updated, needs to be redownloaded
        if (content.download_status() == DownloadStatus::DOWNLOADED){
            content.download_status(DownloadStatus::NEEDS_DOWNLOAD);
        }
        
        content.updated(updated);
        if (mime_type.find("pdf") != std::string::npos){
            content.type(CONTENT_TYPE_PDF);
        } else if (mime_type.find("image") != std::string::npos){
            content.type(CONTENT_TYPE_IMAGE);
        } else if (mime_type.find("video") != std::string::npos){
            content.type(CONTENT_TYPE_VIDEO);
        } else {
            content.type(CONTENT_TYPE_UNKNOWN);
        }
        
        content_repository_->Replace(content);
    }
    
    std::string PiranhaCMS::TimestampToServerDateString(int64_t timestamp){
        std::chrono::seconds seconds(timestamp);
        std::time_t t = (long)seconds.count();
        char buffer[1024];
        if (std::strftime(buffer, 1024, "%Y-%m-%d/%H-%M-%S", std::localtime(&t))){
            return buffer;
        }
        return "1970-01-01/01-01-01";
    }
    
    int64_t PiranhaCMS::ServerDateStringToTimestamp(std::string date){
        std::tm tm;
        strptime(date.c_str(), "%Y-%m-%d/%H-%M-%S", &tm);
        time_t time = mktime(&tm);
        return time;
    }
    
    void PiranhaCMS::HandleDeletions(Json::Value &deletions, std::string base_path){
        Json::Value deleted_pages = deletions.get("Pages", Json::Value());
        for (int i=0; i < deleted_pages.size(); i++){
            std::string page_id = deleted_pages[i].get("Id", "").asString();
            if (page_id != ""){
                content_repository_->RemoveRelationshipForGuid(page_id);
                page_repository_->Delete(page_id);
            }
        }
        
        Json::Value deleted_content = deletions.get("Content", Json::Value());
        for (int i=0; i < deleted_content.size(); i++){
            std::string content_id = deleted_content[i].get("Id", "").asString();
            Content content = content_repository_->GetContentWithId(content_id);
            
            tin::DeleteFile(base_path + "/" + content.file_name());
            if (content_id != ""){
                content_repository_->Delete(content_id);
            }
        }
    }
    
    void PiranhaCMS::DownloadContentWithId(std::string content_id){
        Content content = content_repository()->GetContentWithId(content_id);
        DownloadContent(content);
    }
    
    void PiranhaCMS::DownloadContent(Content content){
        content.download_status(DownloadStatus::NEEDS_DOWNLOAD);
        content_repository()->Replace(content);
        std::string content_url = site_url_ + "/" + content.content_url();
        std::string content_id = content.guid();
        DownloadFileFromURL(content_url, false, base_path_ + "/" + content.file_name(), content_id, [content_id, this](bool success, std::string url, std::string filename, std::string guid, std::string string_data){
            if (success){
                Content content = content_repository_->GetContentWithId(content_id);
                content.download_status(DownloadStatus::DOWNLOADED);
                content_repository_->Replace(content);
            }
        });
    }
    
    void PiranhaCMS::DownloadContentForPage(Page &page){
#ifdef THREADED
        auto content_ids = content_repository_->ContentIdsForGuid(page.guid());
        for (auto content_id : content_ids){
            Content content = content_repository_->GetContentWithId(content_id);
            if (content.download_status() == DownloadStatus::NEEDS_DOWNLOAD){
                thread_pool_->enqueue(&PiranhaCMS::DownloadContent, this, content);
            }
        }
        thread_pool_->WaitUntilEmpty();
#endif
    }
    
    bool PiranhaCMS::DownloadAllUnfinishedContent(){
        auto unfinished_content = content_repository_->UnfinishedContent();
        if (unfinished_content.size() == 0){
            return true;
        }
        for (Content content : unfinished_content){
            DownloadContent(content);
        }
        return false;
    }
}