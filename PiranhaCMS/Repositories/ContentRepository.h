//
//  ContentRepository.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _ContentRepository_H_
#define _ContentRepository_H_

#include <list>
#include <vector>
#include <string>

#include "Repository.h"
#include "../Models/Content.h"

namespace tin{
    class ContentRepository : Repository{
        public:
            ContentRepository(Database *database, PiranhaCMS *cms) : Repository(database, cms) {}
            void CreateTable();
        
            void RemoveRelationshipForGuid(std::string guid);
            void RemoveRelationshipForContentGuid(std::string guid);
        
            void AddRelationshipForGuid(std::string guid, std::string content_id);
        
            Content GetContentWithId(std::string content_id);
            std::vector<Content> GetContentsForType(ContentType content_type);
            std::vector<Content> GetContentForRelationshipWithId(std::string guid);
            std::vector<Content> GetContentForRelationshipWithId(std::string page_id, ContentType type);
        
            std::vector<Content> GetDownloadedContentForRelationshipWithId(std::string guid);
            std::vector<Content> GetDownloadedContentForRelationshipWithId(std::string page_id, ContentType type);
        
            std::vector<std::string> ContentIdsForGuid(std::string guid);
            std::list<std::string> AllContentIds();
        
            void Replace(Content content);
            void Delete(std::string guid);
        
            std::vector<Content> UnfinishedContent();
            
            // Content that is not being used by any pages
            std::vector<std::string> UnusedContentIds();
        private:
            std::vector<Content> ContentsFromSQLString(std::string sql_string);
            Content ContentFromSQLString(std::string sql_string);
            static Content ContentFromStatement(sqlite3_stmt *statement);
    };
} // tin
#endif
