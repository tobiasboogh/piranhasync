//
//  PageRepository.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _PageRepository_H_
#define _PageRepository_H_
#include "Repository.h"
#include "../Models/Page.h"
#include "../Models/Post.h"
namespace tin{
    class PageRepository  : Repository{
        public:
            PageRepository(Database *database, PiranhaCMS *cms) : Repository(database, cms) {}
            void CreateTable();
        
            void Replace(Page page);
            void Delete(std::string guid);
            Page GetPageWithId(std::string page_id);
        
            std::vector<Page> GetChildPagesForPageWithId(std::string page_id);
            std::vector<Page> GetPagesWithTemplate(std::string template_name);
        
            void ToggleSyncForPageWithId(std::string guid, bool recursive);
            void SetSyncForPageWithId(std::string guid, bool sync, bool recursive);
        
            void AddPost(Page page, Post post);
            void RemovePost(Page page, Post post);
            
            int PagePostConnections(Post post);
        private:
            std::vector<Page> PagesFromSQLString(std::string sql_string);
            Page PageFromSQLString(std::string sql_string);
            static void PageFromStatement(Page &page, sqlite3_stmt *statement);
        
            void SetSyncForPage(Page &page, bool sync, bool recursive);
    
    };
} // tin
#endif
