//
//  ContentRepository.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "ContentRepository.h"
#include <set>

namespace tin {
    void ContentRepository::CreateTable(){
        std::string create_guid_content = "CREATE TABLE IF NOT EXISTS GuidContent (Guid TEXT NOT NULL, ContentId TEXT NOT NULL)";
        std::string create_content = "CREATE TABLE IF NOT EXISTS Content (Id TEXT NOT NULL PRIMARY KEY, MimeType TEXT, FileName TEXT, ContentURL TEXT, ThumbnailURL TEXT, DisplayName TEXT, Description TEXT, DownloadStatus INTEGER, Type INTEGER, Updated TEXT, Ignore INTEGER, Flagged INTEGER)";
        
        database_->ExecuteSqlQuery(create_content);
        database_->ExecuteSqlQuery(create_guid_content);
    }
    
    void ContentRepository::RemoveRelationshipForGuid(std::string guid){
        std::string sql_string = "DELETE FROM GuidContent WHERE Guid='" + guid + "'";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    void ContentRepository::RemoveRelationshipForContentGuid(std::string guid){
        std::string sql_string = "DELETE FROM GuidContent WHERE ContentId='" + guid + "'";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    void ContentRepository::AddRelationshipForGuid(std::string guid, std::string content_id){
        std::string sql_string = "INSERT OR REPLACE INTO GuidContent ('Guid', 'ContentId') VALUES ('" + guid + "', '" + content_id + "')";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    Content ContentRepository::ContentFromStatement(sqlite3_stmt *statement){
        Content content;
        
        std::string content_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
        std::string mime_type(reinterpret_cast<const char *>(sqlite3_column_text(statement, 1)));
        std::string filename(reinterpret_cast<const char *>(sqlite3_column_text(statement, 2)));
        std::string content_url(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
        std::string thumbnail_url(reinterpret_cast<const char *>(sqlite3_column_text(statement, 4)));
        std::string display_name(reinterpret_cast<const char *>(sqlite3_column_text(statement, 5)));
        std::string description(reinterpret_cast<const char *>(sqlite3_column_text(statement, 6)));
        int download_status = sqlite3_column_int(statement, 7);
        int type = sqlite3_column_int(statement, 8);
        std::string updated(reinterpret_cast<const char *>(sqlite3_column_text(statement, 9)));
        bool ignore = sqlite3_column_int(statement, 10);
        bool flagged = sqlite3_column_int(statement, 11);
        
        content.guid(content_id);
        content.mime_type(mime_type);
        content.file_name(filename);
        content.content_url(content_url);
        content.thumbnail_url(thumbnail_url);
        content.display_name(display_name);
        content.description(description);
        content.download_status(download_status);
        content.type(static_cast<ContentType>(type));
        content.updated(updated);
        content.ignore(ignore);
        content.flagged(flagged);
        return content;
    }
    
    std::vector<Content> ContentRepository::ContentsFromSQLString(std::string sql_string){
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    Content ContentRepository::ContentFromSQLString(std::string sql_string){
        auto contents = ContentsFromSQLString(sql_string);
        if (contents.size() > 0){
            return contents[0];
        }
        return Content();
    }
    
    
    
    Content ContentRepository::GetContentWithId(std::string content_id){
        std::string sql_string("SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM Content WHERE Id='" + content_id + "'");
        return ContentFromSQLString(sql_string);
    }
    
    std::vector<Content> ContentRepository::GetContentsForType(ContentType content_type){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM Content WHERE Type=" + Utilities::NumberToString(content_type);
        
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    std::vector<std::string> ContentRepository::ContentIdsForGuid(std::string guid){
        std::string sql_string("SELECT ContentId FROM GuidContent WHERE Guid='" + guid + "'");

        std::vector<std::string> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            std::string content_id(reinterpret_cast<const char*>(sqlite3_column_text(statement, 0)));
            contents.push_back(content_id);
        });
        return contents;
    }
    
    std::vector<Content> ContentRepository::GetContentForRelationshipWithId(std::string guid, ContentType type){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM GuidContent pc INNER JOIN Content c ON ContentId = Id WHERE Guid='" + guid + "' and Type=" + Utilities::NumberToString(type);
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    std::vector<Content> ContentRepository::GetContentForRelationshipWithId(std::string guid){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM GuidContent pc INNER JOIN Content c ON ContentId = Id WHERE Guid='" + guid + "'";
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    std::vector<Content> ContentRepository::GetDownloadedContentForRelationshipWithId(std::string guid){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM GuidContent pc INNER JOIN Content c ON ContentId = Id WHERE Guid='" + guid + "' AND DownloadStatus=2";
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    std::vector<Content> ContentRepository::GetDownloadedContentForRelationshipWithId(std::string guid, ContentType type){
        std::string sql_string = "SELECT Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM GuidContent pc INNER JOIN Content c ON ContentId = Id WHERE Guid='" + guid + "' and Type=" + Utilities::NumberToString(type) + " AND DownloadStatus=2";
        std::vector<Content> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            Content content = ContentFromStatement(statement);
            contents.push_back(content);
        });
        return contents;
    }
    
    std::list<std::string> ContentRepository::AllContentIds(){
        std::string sql_string("SELECT Id FROM Content");
        std::list<std::string> contents;
        database_->ExecuteEnumerateRows(sql_string, [&contents](sqlite3_stmt *statement){
            std::string content_id(reinterpret_cast<const char*>(sqlite3_column_text(statement, 0)));
            contents.push_back(content_id);
        });
        return contents;
    }
    
    void ContentRepository::Replace(Content content){
        if (content.guid() == ""){
            return;
        }
        char type_buffer[1024];
        sprintf(type_buffer, "%d", (int)content.type());
        char download_status[16];
        sprintf(download_status, "%d", content.download_status());
        std::string sql_string = "INSERT OR REPLACE INTO Content ('Id', 'MimeType', 'FileName', 'ContentURL', 'ThumbnailURL', 'DisplayName', 'Description', 'DownloadStatus', 'Type', 'Updated', 'Ignore', 'Flagged') VALUES ('" +
        content.guid() + "', '" +
        content.mime_type() + "', '" +
        Database::EscapeSQLString
        (content.file_name()) + "', '" +
        content.content_url() + "', '" +
        content.thumbnail_url() + "', '" + Database::EscapeSQLString(content.display_name()) + "', '" +
        Database::EscapeSQLString(content.description()) + "', '" +
        download_status + "', '" +
        std::string(type_buffer) + "', '" +
        content.updated() + "', '" +
        (content.ignore() ? "1" : "0") + "', '" +
        (content.flagged() ? "1" : "0") + "')";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    void ContentRepository::Delete(std::string guid){
        RemoveRelationshipForContentGuid(guid);
        std::string sql_string = "DELETE FROM Content WHERE Id='" + guid + "'";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    std::vector<Content> ContentRepository::UnfinishedContent(){
        std::string sql_string_incorrect_status_content = "SELECT Content.Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM Page JOIN GuidContent ON GuidContent.Guid==Page.Id JOIN Content ON Content.Id==ContentId WHERE Page.Sync==1 AND Content.DownloadStatus==0";
        std::string sql_string = "SELECT Content.Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM Content JOIN GuidContent ON GuidContent.ContentId=Content.Id JOIN Page ON Page.Id=Guid WHERE DownloadStatus=1 AND Page.Sync=1 GROUP BY Content.Id";
        std::string post_content_sql_string = "SELECT Content.Id, MimeType, FileName, ContentURL, ThumbnailURL, DisplayName, Description, DownloadStatus, Type, Updated, Ignore, Flagged FROM Content JOIN GuidContent ON GuidContent.ContentId=Content.Id JOIN Post ON Post.Id=Guid WHERE DownloadStatus=1 AND Post.Sync=1 GROUP BY Content.Id";
        std::string flagged_content_sql_string = "SELECT * FROM Content WHERE Flagged=1 AND DownloadStatus=1";
        
        std::vector<Content> contents = ContentsFromSQLString(sql_string);
        std::vector<Content> post_contents = ContentsFromSQLString(post_content_sql_string);
        std::vector<Content> flagged_contents = ContentsFromSQLString(flagged_content_sql_string);
        std::vector<Content> incorrect_status_content = ContentsFromSQLString(sql_string_incorrect_status_content);
        contents.insert(contents.end(), post_contents.begin(), post_contents.end());
        contents.insert(contents.end(), flagged_contents.begin(), flagged_contents.end());
        contents.insert(contents.end(), incorrect_status_content.begin(), incorrect_status_content.end());
        return contents;
    }
    
    std::vector<std::string> ContentRepository::UnusedContentIds(){
        std::string sql = "SELECT ContentId FROM Page JOIN GuidContent ON Guid=Page.Id JOIN Content ON ContentId=Content.Id WHERE Sync=0 AND DownloadStatus==2 AND Flagged=0 AND Content.Id NOT IN (SELECT ContentId FROM Page JOIN GuidContent ON Guid=Page.Id JOIN Content ON ContentId=Content.Id WHERE Sync=1 AND DownloadStatus==2 AND Flagged=0 GROUP BY ContentId) GROUP By ContentId";
        std::string post_sql = "SELECT ContentId FROM Post JOIN GuidContent ON Guid=Post.Id JOIN Content ON ContentId=Content.Id WHERE Sync=0 AND DownloadStatus==2 AND Flagged=0 AND Content.Id NOT IN (SELECT ContentId FROM Post JOIN GuidContent ON Guid=Post.Id JOIN Content ON ContentId=Content.Id WHERE Sync=1 AND DownloadStatus==2 AND Flagged=0 GROUP BY ContentId) GROUP By ContentId";
        std::set<std::string> content_id_set;
        database_->ExecuteEnumerateRows(sql, [&content_id_set](sqlite3_stmt *statement){
            std::string content_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
            content_id_set.insert(content_id);
        });
        database_->ExecuteEnumerateRows(post_sql, [&content_id_set](sqlite3_stmt *statement){
            std::string content_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
            content_id_set.insert(content_id);
        });
        std::vector<std::string> content_ids(content_id_set.begin(), content_id_set.end());
        return content_ids;
    }
}