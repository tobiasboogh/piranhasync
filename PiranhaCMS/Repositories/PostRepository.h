//
//  PostRepository.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _PostRepository_H_
#define _PostRepository_H_
#include "Repository.h"
#include "../Models/Post.h"
namespace tin{
    class PostRepository : Repository {
        public:
            PostRepository(Database *database, PiranhaCMS *cms) : Repository(database, cms) {}
            void CreateTable();
        
            void Replace(Post page);
            void Delete(std::string guid);
            Post GetWithId(std::string guid);
        
            std::vector<Post> GetPostsWithTemplate(std::string template_name);
            void SetSyncForPostWithId(std::string guid, bool sync);
            void ToggleSyncForPostWithId(std::string guid);
        
            std::vector<Post> GetPostsForPage(std::string page_guid);
        
            std::vector<Post> GetPostWhereExtensionContains(std::string search);
        private:
            std::vector<Post> PostsFromSQLString(std::string sql_string);
            Post PostFromSQLString(std::string sql_string);
            static void PostFromStatement(Post &page, sqlite3_stmt *statement);
    };
} // tin
#endif
