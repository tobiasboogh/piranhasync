//
//  SettingsRepository.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "SettingsRepository.h"

namespace tin {
    void SettingsRepository::CreateTable(){
        Settings settings;
        std::string create_settings = "CREATE TABLE IF NOT EXISTS Settings (LatestUpdateTimeStamp TEXT NOT NULL default '" + settings.latest_update_timestamp() + "', Language TEXT NOT NULL default '" + settings.language() + "')";
        database_->ExecuteSqlQuery(create_settings);
    }
    
    Settings SettingsRepository::GetSettings(){
        Settings settings;
        int count = 0;
        std::string sql = "SELECT count(1) FROM Settings";
        database_->ExecuteEnumerateRows(sql, [&count](sqlite3_stmt *statement){
            count = sqlite3_column_int(statement, 0);
        });
        
        if (count == 0){
            UpdateSettings(settings);
        } else {
            sql = "SELECT LatestUpdateTimeStamp, Language FROM Settings";
            std::string timestamp, language;
            database_->ExecuteEnumerateRows(sql, [&timestamp, &language](sqlite3_stmt *statement){
                const unsigned char *str_timestamp = sqlite3_column_text(statement, 0);
                if (str_timestamp != nullptr){
                    timestamp = std::string(reinterpret_cast<const char *>(str_timestamp));
                }
                
                const unsigned char *str_language = sqlite3_column_text(statement, 1);
                if (str_language != nullptr){
                    language = std::string(reinterpret_cast<const char *>(str_language));
                }
            });
            
            settings = Settings(timestamp, language);
            return settings;
        }
        return settings;
    }
    
    void SettingsRepository::UpdateSettings(Settings settings){
        int count = 0;
        std::string sql = "SELECT count(1) FROM Settings";
        database_->ExecuteEnumerateRows(sql, [&count](sqlite3_stmt *statement){
            count = sqlite3_column_int(statement, 0);
        });
        
        std::string sql_query;
        
        if (count == 0){
            sql_query = "INSERT INTO Settings (LatestUpdateTimeStamp, Language) VALUES ('" + settings.latest_update_timestamp() + "', '" + settings.language() + "')";
        } else {
            sql_query = "UPDATE Settings SET LatestUpdateTimeStamp='" + settings.latest_update_timestamp() + "', Language='" + settings.language() + "'";
        }
        
        database_->ExecuteSqlQuery(sql_query);
    }
}