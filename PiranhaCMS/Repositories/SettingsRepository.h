//
//  SettingsRepository.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _SettingsRepository_H_
#define _SettingsRepository_H_
#include "Repository.h"
#include "../Models/Settings.h"

namespace tin{
    class SettingsRepository :Repository{
        public:
            SettingsRepository(Database *database, PiranhaCMS *cms) : Repository(database, cms) {}
            void CreateTable();
        
            Settings                    GetSettings();
            void                        UpdateSettings(Settings settings);
        private:
    };
} // tin
#endif
