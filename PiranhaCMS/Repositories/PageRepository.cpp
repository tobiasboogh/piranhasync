//
//  PageRepository.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "PageRepository.h"
#include "PiranhaCMS.h"
namespace tin {
    void PageRepository::CreateTable(){
        std::string create = "CREATE TABLE IF NOT EXISTS Page (Id TEXT NOT NULL PRIMARY KEY, Parentid TEXT, Title TEXT, Regions TEXT, Sync INTEGER, OrderId INTEGER, TemplateName TEXT, Extensions TEXT, IsHidden INT)";
        database_->ExecuteSqlQuery(create);
        
        std::string page_post_rel_table = "CREATE TABLE IF NOT EXISTS PagePost (PageId TEXT NOT NULL, PostId TEXT NOT NULL)";
        database_->ExecuteSqlQuery(page_post_rel_table);
        
        int version = database_->GetUserVersion();
        if (version <= 1){
            std::string add_page_extension = "ALTER TABLE Page ADD COLUMN Extensions TEXT";
            database_->ExecuteSqlQuery(add_page_extension);
        }
    }
    
    void PageRepository::Replace(Page page){
        char order_id[256];
        sprintf(order_id, "%d", page.order_id());
        
        std::string update_page_string = "INSERT OR REPLACE INTO Page ('Id', 'Parentid', 'Title', 'Regions', 'Sync', 'OrderId', 'TemplateName', 'Extensions', 'IsHidden') VALUES ('" + page.guid() + "', '" + page.parent_id() + "', '" + Database::EscapeSQLString(page.title()) + "', '" + Database::EscapeSQLString(page.regions()) + "', '" + (page.sync() ? "1" : "0") + "', '" + std::string(order_id) + "', '" + Database::EscapeSQLString(page.template_name()) + "', '" + Database::EscapeSQLString(page.extensions()) + "', '" + (page.is_hidden() ? "1" : "0")+ "')";
        database_->ExecuteSqlQuery(update_page_string);
    }
    
    void PageRepository::Delete(std::string guid){
        std::string sql_string = "DELETE FROM Page WHERE Id='" + guid + "'";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    Page PageRepository::PageFromSQLString(std::string sql_string){
        Page page;
        database_->ExecuteEnumerateRows(sql_string, [&page](sqlite3_stmt *statement){
            PageFromStatement(page, statement);
        });
        return page;
    }
    
    std::vector<Page> PageRepository::PagesFromSQLString(std::string sql_string){
        std::vector<Page> pages;
        database_->ExecuteEnumerateRows(sql_string, [&pages](sqlite3_stmt *statement){
            Page page;
            PageFromStatement(page, statement);
            pages.push_back(page);
        });
        return pages;
    }
    
    Page PageRepository::GetPageWithId(std::string page_id){
        std::string sql_string = "SELECT Id, ParentId, Title, Regions, Sync, OrderId, TemplateName, Extensions, IsHidden FROM Page WHERE Id='" + page_id + "' ORDER BY OrderId ASC";
        return PageFromSQLString(sql_string);
    }
    
    void PageRepository::PageFromStatement(Page &page, sqlite3_stmt *statement){
        std::string page_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
        std::string parent_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 1)));
        std::string title(reinterpret_cast<const char *>(sqlite3_column_text(statement, 2)));
        std::string regions(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
        bool sync = sqlite3_column_int(statement, 4);
        int order_id = sqlite3_column_int(statement, 5);
        std::string template_name(reinterpret_cast<const char *>(sqlite3_column_text(statement, 6)));
        std::string extensions(reinterpret_cast<const char *>(sqlite3_column_text(statement, 7)));
        int is_hidden = sqlite3_column_int(statement, 8);
        
        page.title(title);
        page.guid(page_id);
        page.parent_id(parent_id);
        page.regions(regions);
        page.sync(sync);
        page.order_id(order_id);
        page.template_name(template_name);
        page.extensions(extensions);
        page.is_hidden(is_hidden);
    }
    
    std::vector<Page> PageRepository::GetChildPagesForPageWithId(std::string page_id){
        std::string sql_string = "SELECT * FROM Page WHERE Parentid = '" + page_id + "' AND IsHidden=0 ORDER BY OrderId ASC";
        return PagesFromSQLString(sql_string);
    }
    
    std::vector<Page> PageRepository::GetPagesWithTemplate(std::string template_name){
        std::string sql_string = "SELECT * FROM Page WHERE TemplateName = '" + template_name + "'";
        return PagesFromSQLString(sql_string);
    }
    
    void PageRepository::ToggleSyncForPageWithId(std::string guid, bool recursive){
        Page page = this->GetPageWithId(guid);
        this->SetSyncForPage(page, !page.sync(), recursive);
        this->Replace(page);
    }
    
    void PageRepository::SetSyncForPage(Page &page, bool sync, bool recursive){
        page.sync(sync);
        if (page.sync() == true){
            auto contents = cms_->content_repository()->GetContentForRelationshipWithId(page.guid());
            for (auto content : contents){
                if (content.download_status() == DownloadStatus::NOT_DOWNLOADED){
                    content.download_status(DownloadStatus::NEEDS_DOWNLOAD);
                }
                cms_->content_repository()->Replace(content);
            }
        }
        if (recursive){
            auto child_pages = this->GetChildPagesForPageWithId(page.guid());
            for (auto child_page : child_pages){
                this->SetSyncForPageWithId(child_page.guid(), sync, recursive);
            }
        }
        if (cms_->delegate() != nullptr){
            cms_->delegate()->DidToggleSyncForPage(page);
        }
    }
    
    void PageRepository::SetSyncForPageWithId(std::string guid, bool sync, bool recursive){
        Page page = this->GetPageWithId(guid);
        this->SetSyncForPage(page, sync, recursive);
        this->Replace(page);
    }
    
    void PageRepository::AddPost(Page page, Post post){
        if (page.guid() == EMPTY_GUID || post.guid() == EMPTY_GUID){
            std::cout << "Invalid guid" << std::endl;
            return;
        }
        std::string sql = "INSERT OR REPLACE INTO PagePost ('PageId', 'PostId') VALUES ('" + page.guid() + "','" + post.guid() + "')";
        this->database_->ExecuteSqlQuery(sql);
    }
    
    void PageRepository::RemovePost(Page page, Post post){
        std::string sql = "DELETE FROM PagePost Where PageId='" + page.guid() + "' AND PostId='" + post.guid() + "'";
        this->database_->ExecuteSqlQuery(sql);
    }
    
    int PageRepository::PagePostConnections(Post post){
        std::string sql = "SELECT COUNT(PostId) FROM PagePost WHERE PostId='" + post.guid() + "'";
        int count = 0;
        this->database_->ExecuteEnumerateRows(sql,[&count](sqlite3_stmt *statement){
            count = sqlite3_column_int(statement, 0);
        });
        return count;
    }
}