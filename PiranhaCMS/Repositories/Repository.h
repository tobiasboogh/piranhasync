//
//  Repository.h
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#ifndef _Repository_H_
#define _Repository_H_

#include "Database.h"
#include "Utilities.h"
namespace tin{
    class PiranhaCMS;
    class Repository {
        public:
            virtual ~Repository() {};
            Repository(Database *database, PiranhaCMS *cms) : database_(database), cms_(cms) {};
            virtual void CreateTable() = 0;
        protected:
            Database *database_;
            PiranhaCMS *cms_;

        private:
            Repository() { };
    };
} // tin
#endif
