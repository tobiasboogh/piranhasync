//
//  PostRepository.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "PostRepository.h"
#include "PiranhaCMS.h"

namespace tin {
    std::mutex t;
    
    void PostRepository::CreateTable(){
        std::string create_posts = "CREATE TABLE IF NOT EXISTS Post (Id TEXT NOT NULL PRIMARY KEY, Parentid TEXT, Title TEXT, Regions TEXT, Sync INTEGER, OrderId INTEGER, TemplateName TEXT, Extensions TEXT)";
        database_->ExecuteSqlQuery(create_posts);
    }
    
    void PostRepository::Replace(Post post){
        char order_id[256];
        sprintf(order_id, "%d", post.order_id());
        std::string update_page_string = "INSERT OR REPLACE INTO Post ('Id', 'Parentid', 'Title', 'Regions', 'Sync', 'OrderId', 'TemplateName', 'Extensions') VALUES ('" + post.guid() + "', '" + post.parent_id() + "', '" + Database::EscapeSQLString(post.title()) + "', '" + Database::EscapeSQLString(post.regions()) + "', '" + (post.sync() ? "1" : "0") + "', '" + std::string(order_id) + "', '" + Database::EscapeSQLString(post.template_name()) + "', '" + Database::EscapeSQLString(post.extensions()) + "')";
        t.lock(); // ?
        t.unlock(); // ?
        database_->ExecuteSqlQuery(update_page_string);
    }
    
    void PostRepository::Delete(std::string guid){
        std::string sql_string = "DELETE FROM Post WHERE Id='" + guid + "'";
        database_->ExecuteSqlQuery(sql_string);
    }
    
    Post PostRepository::GetWithId(std::string guid){
        std::string sql_string = "SELECT Id, ParentId, Title, Regions, Sync, OrderId, TemplateName, Extensions FROM Post WHERE Id='" + guid + "' ORDER BY OrderId ASC";
        return PostFromSQLString(sql_string);

    }

    Post PostRepository::PostFromSQLString(std::string sql_string){
        Post post;
        database_->ExecuteEnumerateRows(sql_string, [&post](sqlite3_stmt *statement){
            PostFromStatement(post, statement);
        });
        return post;
    }
    
    std::vector<Post> PostRepository::PostsFromSQLString(std::string sql_string){
        std::vector<Post> posts;
        database_->ExecuteEnumerateRows(sql_string, [&posts](sqlite3_stmt *statement){
            Post post;
            PostFromStatement(post, statement);
            posts.push_back(post);
        });
        return posts;
    }
    
    void PostRepository::PostFromStatement(Post &post, sqlite3_stmt *statement){
        std::string guid(reinterpret_cast<const char *>(sqlite3_column_text(statement, 0)));
        std::string parent_id(reinterpret_cast<const char *>(sqlite3_column_text(statement, 1)));
        std::string title(reinterpret_cast<const char *>(sqlite3_column_text(statement, 2)));
        std::string regions(reinterpret_cast<const char *>(sqlite3_column_text(statement, 3)));
        bool sync = sqlite3_column_int(statement, 4);
        int order_id = sqlite3_column_int(statement, 5);
        std::string template_name(reinterpret_cast<const char *>(sqlite3_column_text(statement, 6)));
        std::string extensions(reinterpret_cast<const char *>(sqlite3_column_text(statement, 7)));
        
        post.title(title);
        post.guid(guid);
        post.parent_id(parent_id);
        post.regions(regions);
        post.sync(sync);
        post.order_id(order_id);
        post.template_name(template_name);
        post.extensions(extensions);
    }
    
    std::vector<Post> PostRepository::GetPostsWithTemplate(std::string template_name){
        std::string sql_string = "SELECT * FROM Post WHERE TemplateName = '" + template_name + "' ORDER BY Title";
        return PostsFromSQLString(sql_string);
    }
    
    void PostRepository::SetSyncForPostWithId(std::string guid, bool sync){
        Post post = this->GetWithId(guid);
        post.sync(sync);
        if (post.sync()){
            auto contents = cms_->content_repository()->GetContentForRelationshipWithId(post.guid());
            for (auto content : contents){
                if (content.download_status() == DownloadStatus::NOT_DOWNLOADED){
                    content.download_status(DownloadStatus::NEEDS_DOWNLOAD);
                }
                cms_->content_repository()->Replace(content);
            }
        }
        this->Replace(post);
    }
    
    void PostRepository::ToggleSyncForPostWithId(std::string guid){
        Post post = this->GetWithId(guid);
        this->SetSyncForPostWithId(post.guid(), !post.sync());
    }
    
    std::vector<Post> PostRepository::GetPostWhereExtensionContains(std::string search){
        std::string sql = "SELECT * FROM Post WHERE Extensions LIKE " + search + " ORDER BY Title";
        std::vector<Post> posts;
        database_->ExecuteEnumerateRows(sql, [&posts](sqlite3_stmt *statement){
            Post post;
            PostFromStatement(post, statement);
            posts.push_back(post);
        });
        return posts;
    }
    
    std::vector<Post> PostRepository::GetPostsForPage(std::string page_guid){
        std::string sql = "SELECT * FROM ";
    }
}