//
//  Database.cpp
//  PiranhaCMS
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#include "Database.h"

namespace tin {
    bool Database::CheckSqliteError(int result, sqlite3 *database){
        if (result != SQLITE_OK){
            std::cout << sqlite3_errmsg(database) << std::endl;
            return false;
        }
        return true;
    }

    std::string Database::EscapeSQLString(const std::string& input) {
        std::ostringstream ss;
        for (auto iter = input.cbegin(); iter != input.cend(); iter++) {
            switch (*iter) {
                case '\'': ss << "''"; break;
                default: ss << *iter; break;
            }
        }
        return ss.str();
    }
    
    Database::Database(std::string database_path){
        int result = sqlite3_config(SQLITE_CONFIG_SERIALIZED);
        CheckSqliteError(result, database_);
        result = sqlite3_initialize();
        if (!CheckSqliteError(result, database_)){
            std::cout << "ERROR INITILIAZING SQLITE" << std::endl;
        }
        if (!CheckSqliteError(sqlite3_open(database_path.c_str(), &database_), database_)){
            std::cout << "ERROR OPENING DATABASE" << std::endl;
        }
    }
    
    bool Database::ExecuteSqlQuery(std::string sql_string){
        char *errmsg = nullptr;
        if (!CheckSqliteError(sqlite3_exec(database_, sql_string.c_str(), nullptr, nullptr, &errmsg), database_)){
            std::cout << errmsg << std::endl;
            std::cout << sql_string << std::endl;
            return true;
        }
        return false;
    }
    
    void Database::SetUserVersion(int version){
        char buffer[512];
        sprintf(buffer, "PRAGMA user_version=%d;", version);
        std::string sql(buffer);
        ExecuteSqlQuery(sql);
    }
    
    int Database::GetUserVersion(){
        int version = 0;
        
        std::string sql = "PRAGMA user_version;";
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare_v2(database_, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database_)){
            std::cout << "Error with: " << sql << std::endl;
        } else {
            int step = 0;
            while ((step = sqlite3_step(statement)) == SQLITE_ROW){
                version = sqlite3_column_int(statement, 0);
            };
            sqlite3_finalize(statement);
        }
        return version;
    }
    
    bool Database::ExecuteEnumerateRows(std::string sql, std::function<void(sqlite3_stmt *statement)> callback){
        sqlite3_stmt *statement;
        if (!CheckSqliteError(sqlite3_prepare_v2(database_, sql.c_str(), (int)(sql.length() + 1), &statement, nullptr), database_)){
            std::cout << "Error with: " << sql << std::endl;
            return false;
        }
        int step = 0;
        while ((step = sqlite3_step(statement)) == SQLITE_ROW){
            callback(statement);
        };
        sqlite3_finalize(statement);
        return true;
    }
}