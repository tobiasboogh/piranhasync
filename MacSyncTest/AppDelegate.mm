//
//  AppDelegate.m
//  MacSyncTest
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import "AppDelegate.h"
#import "TINPiranahaCMSUtils.h"
@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    [[TINPiranahaCMSUtils getInstance] setSite:@"http://kraftappen.test.bybrick.com" andDelegate:nil];
    [[TINPiranahaCMSUtils getInstance] getChangesWithCompletionBlock:^(bool) {
        NSLog(@"Done!");
    }];

}

@end
