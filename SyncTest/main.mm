//
//  main.m
//  SyncTest
//
//  Created by Tobias Boogh on 07/04/14.
//  Copyright (c) 2014 Tobias Boogh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TINPiranahaCMSUtils.h"
int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);
        // insert code here...
        [[TINPiranahaCMSUtils getInstance] setSite:@"http://kraftappen.test.bybrick.com" andDelegate:nil];
        [[TINPiranahaCMSUtils getInstance] getChangesWithCompletionBlock:^(bool) {
            NSLog(@"Done!");
            dispatch_semaphore_signal(sema);
        }];
        
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    return 0;
}

